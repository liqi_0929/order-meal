package org.linlinjava.litemall.wx.vo;

import java.time.LocalDateTime;

public class TimeLineVo {

        private Integer id;
        private Integer userId;
        private String content;
        private String[] picUrls;
        private Long thumbUp;
        private Long lookNumber;
        private Boolean isAdmin;
        private LocalDateTime addTime;
        private String nickname;
        private String avatar;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public Integer getUserId() {
                return userId;
        }

        public void setUserId(Integer userId) {
                this.userId = userId;
        }

        public String getContent() {
                return content;
        }

        public void setContent(String content) {
                this.content = content;
        }

        public String[] getPicUrls() {
                return picUrls;
        }

        public void setPicUrls(String[] picUrls) {
                this.picUrls = picUrls;
        }

        public Long getThumbUp() {
                return thumbUp;
        }

        public void setThumbUp(Long thumbUp) {
                this.thumbUp = thumbUp;
        }

        public Long getLookNumber() {
                return lookNumber;
        }

        public void setLookNumber(Long lookNumber) {
                this.lookNumber = lookNumber;
        }

        public Boolean getAdmin() {
                return isAdmin;
        }

        public void setAdmin(Boolean admin) {
                isAdmin = admin;
        }

        public LocalDateTime getAddTime() {
                return addTime;
        }

        public void setAddTime(LocalDateTime addTime) {
                this.addTime = addTime;
        }

        public String getNickname() {
                return nickname;
        }

        public void setNickname(String nickname) {
                this.nickname = nickname;
        }

        public String getAvatar() {
                return avatar;
        }

        public void setAvatar(String avatar) {
                this.avatar = avatar;
        }
}
