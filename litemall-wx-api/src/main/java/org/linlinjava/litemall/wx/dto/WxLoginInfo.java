package org.linlinjava.litemall.wx.dto;

public class WxLoginInfo {
    private String wxCode;
    private UserInfo userInfo;

    public String getCode() {
        return wxCode;
    }

    public void setCode(String code) {
        this.wxCode = code;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
