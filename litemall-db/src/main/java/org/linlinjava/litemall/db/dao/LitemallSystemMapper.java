package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallSystem;
import org.linlinjava.litemall.db.example.LitemallSystemExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallSystemMapper {
    /**
     * 系统配置表
     */
    long countByExample(LitemallSystemExample example);

    /**
     * 系统配置表
     */
    int deleteByExample(LitemallSystemExample example);

    /**
     * 系统配置表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 系统配置表
     */
    int insert(LitemallSystem record);

    /**
     * 系统配置表
     */
    int insertSelective(LitemallSystem record);

    /**
     * 系统配置表
     */
    LitemallSystem selectOneByExample(LitemallSystemExample example);

    /**
     * 系统配置表
     */
    LitemallSystem selectOneByExampleSelective(@Param("example") LitemallSystemExample example, @Param("selective") LitemallSystem.Column ... selective);

    /**
     * 系统配置表
     */
    List<LitemallSystem> selectByExampleSelective(@Param("example") LitemallSystemExample example, @Param("selective") LitemallSystem.Column ... selective);

    /**
     * 系统配置表
     */
    List<LitemallSystem> selectByExample(LitemallSystemExample example);

    /**
     * 系统配置表
     */
    LitemallSystem selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallSystem.Column ... selective);

    /**
     * 系统配置表
     */
    LitemallSystem selectByPrimaryKey(Integer id);

    /**
     * 系统配置表
     */
    LitemallSystem selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 系统配置表
     */
    int updateByExampleSelective(@Param("record") LitemallSystem record, @Param("example") LitemallSystemExample example);

    /**
     * 系统配置表
     */
    int updateByExample(@Param("record") LitemallSystem record, @Param("example") LitemallSystemExample example);

    /**
     * 系统配置表
     */
    int updateByPrimaryKeySelective(LitemallSystem record);

    /**
     * 系统配置表
     */
    int updateByPrimaryKey(LitemallSystem record);

    /**
     * 系统配置表
     */
    int logicalDeleteByExample(@Param("example") LitemallSystemExample example);

    /**
     * 系统配置表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}