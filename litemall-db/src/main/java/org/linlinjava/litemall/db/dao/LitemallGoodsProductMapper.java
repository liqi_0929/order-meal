package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallGoodsProduct;
import org.linlinjava.litemall.db.example.LitemallGoodsProductExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallGoodsProductMapper {
    /**
     * 商品货品表
     */
    long countByExample(LitemallGoodsProductExample example);

    /**
     * 商品货品表
     */
    int deleteByExample(LitemallGoodsProductExample example);

    /**
     * 商品货品表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 商品货品表
     */
    int insert(LitemallGoodsProduct record);

    /**
     * 商品货品表
     */
    int insertSelective(LitemallGoodsProduct record);

    /**
     * 商品货品表
     */
    LitemallGoodsProduct selectOneByExample(LitemallGoodsProductExample example);

    /**
     * 商品货品表
     */
    LitemallGoodsProduct selectOneByExampleSelective(@Param("example") LitemallGoodsProductExample example, @Param("selective") LitemallGoodsProduct.Column ... selective);

    /**
     * 商品货品表
     */
    List<LitemallGoodsProduct> selectByExampleSelective(@Param("example") LitemallGoodsProductExample example, @Param("selective") LitemallGoodsProduct.Column ... selective);

    /**
     * 商品货品表
     */
    List<LitemallGoodsProduct> selectByExample(LitemallGoodsProductExample example);

    /**
     * 商品货品表
     */
    LitemallGoodsProduct selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallGoodsProduct.Column ... selective);

    /**
     * 商品货品表
     */
    LitemallGoodsProduct selectByPrimaryKey(Integer id);

    /**
     * 商品货品表
     */
    LitemallGoodsProduct selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 商品货品表
     */
    int updateByExampleSelective(@Param("record") LitemallGoodsProduct record, @Param("example") LitemallGoodsProductExample example);

    /**
     * 商品货品表
     */
    int updateByExample(@Param("record") LitemallGoodsProduct record, @Param("example") LitemallGoodsProductExample example);

    /**
     * 商品货品表
     */
    int updateByPrimaryKeySelective(LitemallGoodsProduct record);

    /**
     * 商品货品表
     */
    int updateByPrimaryKey(LitemallGoodsProduct record);

    /**
     * 商品货品表
     */
    int logicalDeleteByExample(@Param("example") LitemallGoodsProductExample example);

    /**
     * 商品货品表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}