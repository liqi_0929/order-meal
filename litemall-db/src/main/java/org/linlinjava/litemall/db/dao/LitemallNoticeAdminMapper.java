package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallNoticeAdmin;
import org.linlinjava.litemall.db.example.LitemallNoticeAdminExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallNoticeAdminMapper {
    /**
     * 通知管理员表
     */
    long countByExample(LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int deleteByExample(LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 通知管理员表
     */
    int insert(LitemallNoticeAdmin record);

    /**
     * 通知管理员表
     */
    int insertSelective(LitemallNoticeAdmin record);

    /**
     * 通知管理员表
     */
    LitemallNoticeAdmin selectOneByExample(LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    LitemallNoticeAdmin selectOneByExampleSelective(@Param("example") LitemallNoticeAdminExample example, @Param("selective") LitemallNoticeAdmin.Column ... selective);

    /**
     * 通知管理员表
     */
    List<LitemallNoticeAdmin> selectByExampleSelective(@Param("example") LitemallNoticeAdminExample example, @Param("selective") LitemallNoticeAdmin.Column ... selective);

    /**
     * 通知管理员表
     */
    List<LitemallNoticeAdmin> selectByExample(LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    LitemallNoticeAdmin selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallNoticeAdmin.Column ... selective);

    /**
     * 通知管理员表
     */
    LitemallNoticeAdmin selectByPrimaryKey(Integer id);

    /**
     * 通知管理员表
     */
    LitemallNoticeAdmin selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 通知管理员表
     */
    int updateByExampleSelective(@Param("record") LitemallNoticeAdmin record, @Param("example") LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int updateByExample(@Param("record") LitemallNoticeAdmin record, @Param("example") LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int updateByPrimaryKeySelective(LitemallNoticeAdmin record);

    /**
     * 通知管理员表
     */
    int updateByPrimaryKey(LitemallNoticeAdmin record);

    /**
     * 通知管理员表
     */
    int logicalDeleteByExample(@Param("example") LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}