package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallOrderGoods;
import org.linlinjava.litemall.db.example.LitemallOrderGoodsExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallOrderGoodsMapper {
    /**
     * 订单商品表
     */
    long countByExample(LitemallOrderGoodsExample example);

    /**
     * 订单商品表
     */
    int deleteByExample(LitemallOrderGoodsExample example);

    /**
     * 订单商品表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 订单商品表
     */
    int insert(LitemallOrderGoods record);

    /**
     * 订单商品表
     */
    int insertSelective(LitemallOrderGoods record);

    /**
     * 订单商品表
     */
    LitemallOrderGoods selectOneByExample(LitemallOrderGoodsExample example);

    /**
     * 订单商品表
     */
    LitemallOrderGoods selectOneByExampleSelective(@Param("example") LitemallOrderGoodsExample example, @Param("selective") LitemallOrderGoods.Column ... selective);

    /**
     * 订单商品表
     */
    List<LitemallOrderGoods> selectByExampleSelective(@Param("example") LitemallOrderGoodsExample example, @Param("selective") LitemallOrderGoods.Column ... selective);

    /**
     * 订单商品表
     */
    List<LitemallOrderGoods> selectByExample(LitemallOrderGoodsExample example);

    /**
     * 订单商品表
     */
    LitemallOrderGoods selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallOrderGoods.Column ... selective);

    /**
     * 订单商品表
     */
    LitemallOrderGoods selectByPrimaryKey(Integer id);

    /**
     * 订单商品表
     */
    LitemallOrderGoods selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 订单商品表
     */
    int updateByExampleSelective(@Param("record") LitemallOrderGoods record, @Param("example") LitemallOrderGoodsExample example);

    /**
     * 订单商品表
     */
    int updateByExample(@Param("record") LitemallOrderGoods record, @Param("example") LitemallOrderGoodsExample example);

    /**
     * 订单商品表
     */
    int updateByPrimaryKeySelective(LitemallOrderGoods record);

    /**
     * 订单商品表
     */
    int updateByPrimaryKey(LitemallOrderGoods record);

    /**
     * 订单商品表
     */
    int logicalDeleteByExample(@Param("example") LitemallOrderGoodsExample example);

    /**
     * 订单商品表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}