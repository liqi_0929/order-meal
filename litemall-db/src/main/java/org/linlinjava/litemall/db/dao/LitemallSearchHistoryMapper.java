package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallSearchHistory;
import org.linlinjava.litemall.db.example.LitemallSearchHistoryExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallSearchHistoryMapper {
    /**
     * 搜索历史表
     */
    long countByExample(LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int deleteByExample(LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 搜索历史表
     */
    int insert(LitemallSearchHistory record);

    /**
     * 搜索历史表
     */
    int insertSelective(LitemallSearchHistory record);

    /**
     * 搜索历史表
     */
    LitemallSearchHistory selectOneByExample(LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    LitemallSearchHistory selectOneByExampleSelective(@Param("example") LitemallSearchHistoryExample example, @Param("selective") LitemallSearchHistory.Column ... selective);

    /**
     * 搜索历史表
     */
    List<LitemallSearchHistory> selectByExampleSelective(@Param("example") LitemallSearchHistoryExample example, @Param("selective") LitemallSearchHistory.Column ... selective);

    /**
     * 搜索历史表
     */
    List<LitemallSearchHistory> selectByExample(LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    LitemallSearchHistory selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallSearchHistory.Column ... selective);

    /**
     * 搜索历史表
     */
    LitemallSearchHistory selectByPrimaryKey(Integer id);

    /**
     * 搜索历史表
     */
    LitemallSearchHistory selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 搜索历史表
     */
    int updateByExampleSelective(@Param("record") LitemallSearchHistory record, @Param("example") LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int updateByExample(@Param("record") LitemallSearchHistory record, @Param("example") LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int updateByPrimaryKeySelective(LitemallSearchHistory record);

    /**
     * 搜索历史表
     */
    int updateByPrimaryKey(LitemallSearchHistory record);

    /**
     * 搜索历史表
     */
    int logicalDeleteByExample(@Param("example") LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}