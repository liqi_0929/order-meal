package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.example.LitemallOrderExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallOrderMapper {
    /**
     * 订单表
     */
    long countByExample(LitemallOrderExample example);

    /**
     * 订单表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallOrderExample example);

    /**
     * 订单表
     */
    int deleteByExample(LitemallOrderExample example);

    /**
     * 订单表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 订单表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 订单表
     */
    int insert(LitemallOrder record);

    /**
     * 订单表
     */
    int insertSelective(LitemallOrder record);

    /**
     * 订单表
     */
    LitemallOrder selectOneByExample(LitemallOrderExample example);

    /**
     * 订单表
     */
    LitemallOrder selectOneByExampleSelective(@Param("example") LitemallOrderExample example, @Param("selective") LitemallOrder.Column ... selective);

    /**
     * 订单表
     */
    List<LitemallOrder> selectByExampleSelective(@Param("example") LitemallOrderExample example, @Param("selective") LitemallOrder.Column ... selective);

    /**
     * 订单表
     */
    List<LitemallOrder> selectByExample(LitemallOrderExample example);

    /**
     * 订单表
     */
    LitemallOrder selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallOrder.Column ... selective);

    /**
     * 订单表
     */
    LitemallOrder selectByPrimaryKey(Integer id);

    /**
     * 订单表
     */
    LitemallOrder selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 订单表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallOrder record, @Param("example") LitemallOrderExample example);

    /**
     * 订单表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallOrder record, @Param("example") LitemallOrderExample example);

    /**
     * 订单表
     */
    int updateByExampleSelective(@Param("record") LitemallOrder record, @Param("example") LitemallOrderExample example);

    /**
     * 订单表
     */
    int updateByExample(@Param("record") LitemallOrder record, @Param("example") LitemallOrderExample example);

    /**
     * 订单表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallOrder record);

    /**
     * 订单表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallOrder record);

    /**
     * 订单表
     */
    int updateByPrimaryKeySelective(LitemallOrder record);

    /**
     * 订单表
     */
    int updateByPrimaryKey(LitemallOrder record);

    /**
     * 订单表
     */
    int logicalDeleteByExample(@Param("example") LitemallOrderExample example);

    /**
     * 订单表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallOrderExample example);

    /**
     * 订单表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 订单表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}