package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallRegion;
import org.linlinjava.litemall.db.example.LitemallRegionExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallRegionMapper {
    /**
     * 行政区域表
     */
    long countByExample(LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int deleteByExample(LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 行政区域表
     */
    int insert(LitemallRegion record);

    /**
     * 行政区域表
     */
    int insertSelective(LitemallRegion record);

    /**
     * 行政区域表
     */
    LitemallRegion selectOneByExample(LitemallRegionExample example);

    /**
     * 行政区域表
     */
    LitemallRegion selectOneByExampleSelective(@Param("example") LitemallRegionExample example, @Param("selective") LitemallRegion.Column ... selective);

    /**
     * 行政区域表
     */
    List<LitemallRegion> selectByExampleSelective(@Param("example") LitemallRegionExample example, @Param("selective") LitemallRegion.Column ... selective);

    /**
     * 行政区域表
     */
    List<LitemallRegion> selectByExample(LitemallRegionExample example);

    /**
     * 行政区域表
     */
    LitemallRegion selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallRegion.Column ... selective);

    /**
     * 行政区域表
     */
    LitemallRegion selectByPrimaryKey(Integer id);

    /**
     * 行政区域表
     */
    int updateByExampleSelective(@Param("record") LitemallRegion record, @Param("example") LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int updateByExample(@Param("record") LitemallRegion record, @Param("example") LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int updateByPrimaryKeySelective(LitemallRegion record);

    /**
     * 行政区域表
     */
    int updateByPrimaryKey(LitemallRegion record);
}