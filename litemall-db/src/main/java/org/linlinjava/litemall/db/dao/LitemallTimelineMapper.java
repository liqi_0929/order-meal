package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallTimeline;
import org.linlinjava.litemall.db.example.LitemallTimelineExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallTimelineMapper {
    /**
     * 
     */
    long countByExample(LitemallTimelineExample example);

    /**
     * 
     */
    int deleteByExample(LitemallTimelineExample example);

    /**
     * 
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 
     */
    int insert(LitemallTimeline record);

    /**
     * 
     */
    int insertSelective(LitemallTimeline record);

    /**
     * 
     */
    LitemallTimeline selectOneByExample(LitemallTimelineExample example);

    /**
     * 
     */
    LitemallTimeline selectOneByExampleSelective(@Param("example") LitemallTimelineExample example, @Param("selective") LitemallTimeline.Column ... selective);

    /**
     * 
     */
    List<LitemallTimeline> selectByExampleSelective(@Param("example") LitemallTimelineExample example, @Param("selective") LitemallTimeline.Column ... selective);

    /**
     * 
     */
    List<LitemallTimeline> selectByExample(LitemallTimelineExample example);

    /**
     * 
     */
    LitemallTimeline selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallTimeline.Column ... selective);

    /**
     * 
     */
    LitemallTimeline selectByPrimaryKey(Integer id);

    /**
     * 
     */
    LitemallTimeline selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 
     */
    int updateByExampleSelective(@Param("record") LitemallTimeline record, @Param("example") LitemallTimelineExample example);

    /**
     * 
     */
    int updateByExample(@Param("record") LitemallTimeline record, @Param("example") LitemallTimelineExample example);

    /**
     * 
     */
    int updateByPrimaryKeySelective(LitemallTimeline record);

    /**
     * 
     */
    int updateByPrimaryKey(LitemallTimeline record);

    /**
     * 
     */
    int logicalDeleteByExample(@Param("example") LitemallTimelineExample example);

    /**
     * 
     */
    int logicalDeleteByPrimaryKey(Integer id);
}