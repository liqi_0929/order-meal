package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;
import org.linlinjava.litemall.db.dao.LitemallTimelineMapper;
import org.linlinjava.litemall.db.domain.LitemallTimeline;
import org.linlinjava.litemall.db.example.LitemallTimelineExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class LitemallTimelineService {

    @Autowired
    private LitemallTimelineMapper timelineMapper;

    public Integer add(LitemallTimeline timeline) {
        timeline.setAddTime(LocalDateTime.now());
        timeline.setUpdateTime(LocalDateTime.now());
        return timelineMapper.insertSelective(timeline);
    }

    public int count() {
        LitemallTimelineExample example = new LitemallTimelineExample();
        example.or().andDeletedEqualTo(false);
        return (int) timelineMapper.countByExample(example);
    }

    public List<LitemallTimeline> queryList(Integer page, Integer limit, String sort, String order) {
        LitemallTimelineExample example = new LitemallTimelineExample();
        example.or().andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }
        PageHelper.startPage(page, limit);
        return timelineMapper.selectByExample(example);
    }

    public int updateById(LitemallTimeline timeline) {
        timeline.setUpdateTime(LocalDateTime.now());
        return timelineMapper.updateByPrimaryKeySelective(timeline);
    }


    public void deleteById(Integer id) {
        LitemallTimelineExample example = new LitemallTimelineExample();
        example.or().andIdEqualTo(id);
        timelineMapper.logicalDeleteByExample(example);
    }


}
