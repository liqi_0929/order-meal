package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_category
 * Database Table Remarks : 
 *   类目表
 * @author linlinjava
 */
public class LitemallCategory implements Serializable {
    /**
     * litemall_category
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_category
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    /**
     * 类目名称
     */
    private String name;

    /**
     * 类目关键字，以JSON数组格式
     */
    private String keywords;

    /**
     * 类目广告语介绍
     */
    private String desc;

    /**
     * 父类目ID
     */
    private Integer pid;

    /**
     * 类目图标
     */
    private String iconUrl;

    /**
     * 类目图片
     */
    private String picUrl;

    /**
     * 
     */
    private String level;

    /**
     * 排序
     */
    private Byte sortOrder;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * litemall_category
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 类目名称
     * @return name 类目名称
     */
    public String getName() {
        return name;
    }

    /**
     * 类目名称
     * @param name 类目名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 类目关键字，以JSON数组格式
     * @return keywords 类目关键字，以JSON数组格式
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * 类目关键字，以JSON数组格式
     * @param keywords 类目关键字，以JSON数组格式
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    /**
     * 类目广告语介绍
     * @return desc 类目广告语介绍
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 类目广告语介绍
     * @param desc 类目广告语介绍
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 父类目ID
     * @return pid 父类目ID
     */
    public Integer getPid() {
        return pid;
    }

    /**
     * 父类目ID
     * @param pid 父类目ID
     */
    public void setPid(Integer pid) {
        this.pid = pid;
    }

    /**
     * 类目图标
     * @return icon_url 类目图标
     */
    public String getIconUrl() {
        return iconUrl;
    }

    /**
     * 类目图标
     * @param iconUrl 类目图标
     */
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    /**
     * 类目图片
     * @return pic_url 类目图片
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 类目图片
     * @param picUrl 类目图片
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * 
     * @return level 
     */
    public String getLevel() {
        return level;
    }

    /**
     * 
     * @param level 
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /**
     * 排序
     * @return sort_order 排序
     */
    public Byte getSortOrder() {
        return sortOrder;
    }

    /**
     * 排序
     * @param sortOrder 排序
     */
    public void setSortOrder(Byte sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 类目表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 类目表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", keywords=").append(keywords);
        sb.append(", desc=").append(desc);
        sb.append(", pid=").append(pid);
        sb.append(", iconUrl=").append(iconUrl);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", level=").append(level);
        sb.append(", sortOrder=").append(sortOrder);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 类目表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallCategory other = (LitemallCategory) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getKeywords() == null ? other.getKeywords() == null : this.getKeywords().equals(other.getKeywords()))
            && (this.getDesc() == null ? other.getDesc() == null : this.getDesc().equals(other.getDesc()))
            && (this.getPid() == null ? other.getPid() == null : this.getPid().equals(other.getPid()))
            && (this.getIconUrl() == null ? other.getIconUrl() == null : this.getIconUrl().equals(other.getIconUrl()))
            && (this.getPicUrl() == null ? other.getPicUrl() == null : this.getPicUrl().equals(other.getPicUrl()))
            && (this.getLevel() == null ? other.getLevel() == null : this.getLevel().equals(other.getLevel()))
            && (this.getSortOrder() == null ? other.getSortOrder() == null : this.getSortOrder().equals(other.getSortOrder()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()));
    }

    /**
     * 类目表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getKeywords() == null) ? 0 : getKeywords().hashCode());
        result = prime * result + ((getDesc() == null) ? 0 : getDesc().hashCode());
        result = prime * result + ((getPid() == null) ? 0 : getPid().hashCode());
        result = prime * result + ((getIconUrl() == null) ? 0 : getIconUrl().hashCode());
        result = prime * result + ((getPicUrl() == null) ? 0 : getPicUrl().hashCode());
        result = prime * result + ((getLevel() == null) ? 0 : getLevel().hashCode());
        result = prime * result + ((getSortOrder() == null) ? 0 : getSortOrder().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        return result;
    }

    /**
     * litemall_category
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_category
         */
        private final Boolean value;

        /**
         * litemall_category
         */
        private final String name;

        /**
         * 类目表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 类目表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 类目表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 类目表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 类目表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 类目表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_category
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        name("name", "name", "VARCHAR", true),
        keywords("keywords", "keywords", "VARCHAR", false),
        desc("desc", "desc", "VARCHAR", true),
        pid("pid", "pid", "INTEGER", false),
        iconUrl("icon_url", "iconUrl", "VARCHAR", false),
        picUrl("pic_url", "picUrl", "VARCHAR", false),
        level("level", "level", "VARCHAR", true),
        sortOrder("sort_order", "sortOrder", "TINYINT", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false);

        /**
         * litemall_category
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_category
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_category
         */
        private final String column;

        /**
         * litemall_category
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_category
         */
        private final String javaProperty;

        /**
         * litemall_category
         */
        private final String jdbcType;

        /**
         * 类目表
         */
        public String value() {
            return this.column;
        }

        /**
         * 类目表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 类目表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 类目表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 类目表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 类目表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 类目表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 类目表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 类目表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 类目表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 类目表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}