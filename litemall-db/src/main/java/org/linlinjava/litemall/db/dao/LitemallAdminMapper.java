package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallAdmin;
import org.linlinjava.litemall.db.example.LitemallAdminExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallAdminMapper {
    /**
     * 管理员表
     */
    long countByExample(LitemallAdminExample example);

    /**
     * 管理员表
     */
    int deleteByExample(LitemallAdminExample example);

    /**
     * 管理员表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 管理员表
     */
    int insert(LitemallAdmin record);

    /**
     * 管理员表
     */
    int insertSelective(LitemallAdmin record);

    /**
     * 管理员表
     */
    LitemallAdmin selectOneByExample(LitemallAdminExample example);

    /**
     * 管理员表
     */
    LitemallAdmin selectOneByExampleSelective(@Param("example") LitemallAdminExample example, @Param("selective") LitemallAdmin.Column ... selective);

    /**
     * 管理员表
     */
    List<LitemallAdmin> selectByExampleSelective(@Param("example") LitemallAdminExample example, @Param("selective") LitemallAdmin.Column ... selective);

    /**
     * 管理员表
     */
    List<LitemallAdmin> selectByExample(LitemallAdminExample example);

    /**
     * 管理员表
     */
    LitemallAdmin selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallAdmin.Column ... selective);

    /**
     * 管理员表
     */
    LitemallAdmin selectByPrimaryKey(Integer id);

    /**
     * 管理员表
     */
    LitemallAdmin selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 管理员表
     */
    int updateByExampleSelective(@Param("record") LitemallAdmin record, @Param("example") LitemallAdminExample example);

    /**
     * 管理员表
     */
    int updateByExample(@Param("record") LitemallAdmin record, @Param("example") LitemallAdminExample example);

    /**
     * 管理员表
     */
    int updateByPrimaryKeySelective(LitemallAdmin record);

    /**
     * 管理员表
     */
    int updateByPrimaryKey(LitemallAdmin record);

    /**
     * 管理员表
     */
    int logicalDeleteByExample(@Param("example") LitemallAdminExample example);

    /**
     * 管理员表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}