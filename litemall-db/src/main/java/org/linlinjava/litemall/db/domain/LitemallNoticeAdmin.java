package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_notice_admin
 * Database Table Remarks : 
 *   通知管理员表
 * @author linlinjava
 */
public class LitemallNoticeAdmin implements Serializable {
    /**
     * litemall_notice_admin
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_notice_admin
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    /**
     * 通知ID
     */
    private Integer noticeId;

    /**
     * 通知标题
     */
    private String noticeTitle;

    /**
     * 接收通知的管理员ID
     */
    private Integer adminId;

    /**
     * 阅读时间，如果是NULL则是未读状态
     */
    private LocalDateTime readTime;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * litemall_notice_admin
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 通知ID
     * @return notice_id 通知ID
     */
    public Integer getNoticeId() {
        return noticeId;
    }

    /**
     * 通知ID
     * @param noticeId 通知ID
     */
    public void setNoticeId(Integer noticeId) {
        this.noticeId = noticeId;
    }

    /**
     * 通知标题
     * @return notice_title 通知标题
     */
    public String getNoticeTitle() {
        return noticeTitle;
    }

    /**
     * 通知标题
     * @param noticeTitle 通知标题
     */
    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    /**
     * 接收通知的管理员ID
     * @return admin_id 接收通知的管理员ID
     */
    public Integer getAdminId() {
        return adminId;
    }

    /**
     * 接收通知的管理员ID
     * @param adminId 接收通知的管理员ID
     */
    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    /**
     * 阅读时间，如果是NULL则是未读状态
     * @return read_time 阅读时间，如果是NULL则是未读状态
     */
    public LocalDateTime getReadTime() {
        return readTime;
    }

    /**
     * 阅读时间，如果是NULL则是未读状态
     * @param readTime 阅读时间，如果是NULL则是未读状态
     */
    public void setReadTime(LocalDateTime readTime) {
        this.readTime = readTime;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 通知管理员表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 通知管理员表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", noticeId=").append(noticeId);
        sb.append(", noticeTitle=").append(noticeTitle);
        sb.append(", adminId=").append(adminId);
        sb.append(", readTime=").append(readTime);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 通知管理员表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallNoticeAdmin other = (LitemallNoticeAdmin) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getNoticeId() == null ? other.getNoticeId() == null : this.getNoticeId().equals(other.getNoticeId()))
            && (this.getNoticeTitle() == null ? other.getNoticeTitle() == null : this.getNoticeTitle().equals(other.getNoticeTitle()))
            && (this.getAdminId() == null ? other.getAdminId() == null : this.getAdminId().equals(other.getAdminId()))
            && (this.getReadTime() == null ? other.getReadTime() == null : this.getReadTime().equals(other.getReadTime()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()));
    }

    /**
     * 通知管理员表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getNoticeId() == null) ? 0 : getNoticeId().hashCode());
        result = prime * result + ((getNoticeTitle() == null) ? 0 : getNoticeTitle().hashCode());
        result = prime * result + ((getAdminId() == null) ? 0 : getAdminId().hashCode());
        result = prime * result + ((getReadTime() == null) ? 0 : getReadTime().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        return result;
    }

    /**
     * litemall_notice_admin
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_notice_admin
         */
        private final Boolean value;

        /**
         * litemall_notice_admin
         */
        private final String name;

        /**
         * 通知管理员表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 通知管理员表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 通知管理员表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 通知管理员表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 通知管理员表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 通知管理员表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_notice_admin
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        noticeId("notice_id", "noticeId", "INTEGER", false),
        noticeTitle("notice_title", "noticeTitle", "VARCHAR", false),
        adminId("admin_id", "adminId", "INTEGER", false),
        readTime("read_time", "readTime", "TIMESTAMP", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false);

        /**
         * litemall_notice_admin
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_notice_admin
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_notice_admin
         */
        private final String column;

        /**
         * litemall_notice_admin
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_notice_admin
         */
        private final String javaProperty;

        /**
         * litemall_notice_admin
         */
        private final String jdbcType;

        /**
         * 通知管理员表
         */
        public String value() {
            return this.column;
        }

        /**
         * 通知管理员表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 通知管理员表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 通知管理员表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 通知管理员表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 通知管理员表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 通知管理员表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 通知管理员表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 通知管理员表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 通知管理员表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 通知管理员表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}