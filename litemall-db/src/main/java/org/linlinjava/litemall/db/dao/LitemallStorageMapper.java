package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallStorage;
import org.linlinjava.litemall.db.example.LitemallStorageExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallStorageMapper {
    /**
     * 文件存储表
     */
    long countByExample(LitemallStorageExample example);

    /**
     * 文件存储表
     */
    int deleteByExample(LitemallStorageExample example);

    /**
     * 文件存储表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 文件存储表
     */
    int insert(LitemallStorage record);

    /**
     * 文件存储表
     */
    int insertSelective(LitemallStorage record);

    /**
     * 文件存储表
     */
    LitemallStorage selectOneByExample(LitemallStorageExample example);

    /**
     * 文件存储表
     */
    LitemallStorage selectOneByExampleSelective(@Param("example") LitemallStorageExample example, @Param("selective") LitemallStorage.Column ... selective);

    /**
     * 文件存储表
     */
    List<LitemallStorage> selectByExampleSelective(@Param("example") LitemallStorageExample example, @Param("selective") LitemallStorage.Column ... selective);

    /**
     * 文件存储表
     */
    List<LitemallStorage> selectByExample(LitemallStorageExample example);

    /**
     * 文件存储表
     */
    LitemallStorage selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallStorage.Column ... selective);

    /**
     * 文件存储表
     */
    LitemallStorage selectByPrimaryKey(Integer id);

    /**
     * 文件存储表
     */
    LitemallStorage selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 文件存储表
     */
    int updateByExampleSelective(@Param("record") LitemallStorage record, @Param("example") LitemallStorageExample example);

    /**
     * 文件存储表
     */
    int updateByExample(@Param("record") LitemallStorage record, @Param("example") LitemallStorageExample example);

    /**
     * 文件存储表
     */
    int updateByPrimaryKeySelective(LitemallStorage record);

    /**
     * 文件存储表
     */
    int updateByPrimaryKey(LitemallStorage record);

    /**
     * 文件存储表
     */
    int logicalDeleteByExample(@Param("example") LitemallStorageExample example);

    /**
     * 文件存储表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}