package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_log
 * Database Table Remarks : 
 *   操作日志表
 * @author linlinjava
 */
public class LitemallLog implements Serializable {
    /**
     * litemall_log
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_log
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    /**
     * 管理员
     */
    private String admin;

    /**
     * 管理员地址
     */
    private String ip;

    /**
     * 操作分类
     */
    private Integer type;

    /**
     * 操作动作
     */
    private String action;

    /**
     * 操作状态
     */
    private Boolean status;

    /**
     * 操作结果，或者成功消息，或者失败消息
     */
    private String result;

    /**
     * 补充信息
     */
    private String comment;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * litemall_log
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 管理员
     * @return admin 管理员
     */
    public String getAdmin() {
        return admin;
    }

    /**
     * 管理员
     * @param admin 管理员
     */
    public void setAdmin(String admin) {
        this.admin = admin;
    }

    /**
     * 管理员地址
     * @return ip 管理员地址
     */
    public String getIp() {
        return ip;
    }

    /**
     * 管理员地址
     * @param ip 管理员地址
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * 操作分类
     * @return type 操作分类
     */
    public Integer getType() {
        return type;
    }

    /**
     * 操作分类
     * @param type 操作分类
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 操作动作
     * @return action 操作动作
     */
    public String getAction() {
        return action;
    }

    /**
     * 操作动作
     * @param action 操作动作
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * 操作状态
     * @return status 操作状态
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 操作状态
     * @param status 操作状态
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 操作结果，或者成功消息，或者失败消息
     * @return result 操作结果，或者成功消息，或者失败消息
     */
    public String getResult() {
        return result;
    }

    /**
     * 操作结果，或者成功消息，或者失败消息
     * @param result 操作结果，或者成功消息，或者失败消息
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * 补充信息
     * @return comment 补充信息
     */
    public String getComment() {
        return comment;
    }

    /**
     * 补充信息
     * @param comment 补充信息
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 操作日志表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 操作日志表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", admin=").append(admin);
        sb.append(", ip=").append(ip);
        sb.append(", type=").append(type);
        sb.append(", action=").append(action);
        sb.append(", status=").append(status);
        sb.append(", result=").append(result);
        sb.append(", comment=").append(comment);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 操作日志表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallLog other = (LitemallLog) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAdmin() == null ? other.getAdmin() == null : this.getAdmin().equals(other.getAdmin()))
            && (this.getIp() == null ? other.getIp() == null : this.getIp().equals(other.getIp()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getAction() == null ? other.getAction() == null : this.getAction().equals(other.getAction()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getResult() == null ? other.getResult() == null : this.getResult().equals(other.getResult()))
            && (this.getComment() == null ? other.getComment() == null : this.getComment().equals(other.getComment()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()));
    }

    /**
     * 操作日志表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAdmin() == null) ? 0 : getAdmin().hashCode());
        result = prime * result + ((getIp() == null) ? 0 : getIp().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getAction() == null) ? 0 : getAction().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getResult() == null) ? 0 : getResult().hashCode());
        result = prime * result + ((getComment() == null) ? 0 : getComment().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        return result;
    }

    /**
     * litemall_log
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_log
         */
        private final Boolean value;

        /**
         * litemall_log
         */
        private final String name;

        /**
         * 操作日志表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 操作日志表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 操作日志表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 操作日志表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 操作日志表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 操作日志表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_log
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        admin("admin", "admin", "VARCHAR", true),
        ip("ip", "ip", "VARCHAR", false),
        type("type", "type", "INTEGER", true),
        action("action", "action", "VARCHAR", true),
        status("status", "status", "BIT", true),
        result("result", "result", "VARCHAR", true),
        comment("comment", "comment", "VARCHAR", true),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false);

        /**
         * litemall_log
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_log
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_log
         */
        private final String column;

        /**
         * litemall_log
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_log
         */
        private final String javaProperty;

        /**
         * litemall_log
         */
        private final String jdbcType;

        /**
         * 操作日志表
         */
        public String value() {
            return this.column;
        }

        /**
         * 操作日志表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 操作日志表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 操作日志表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 操作日志表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 操作日志表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 操作日志表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 操作日志表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 操作日志表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 操作日志表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 操作日志表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}