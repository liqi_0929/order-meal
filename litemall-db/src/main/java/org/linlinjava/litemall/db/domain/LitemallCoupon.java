package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_coupon
 * Database Table Remarks : 
 *   优惠券信息及规则表
 * @author linlinjava
 */
public class LitemallCoupon implements Serializable {
    /**
     * litemall_coupon
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_coupon
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    /**
     * 优惠券名称
     */
    private String name;

    /**
     * 优惠券介绍，通常是显示优惠券使用限制文字
     */
    private String desc;

    /**
     * 优惠券标签，例如新人专用
     */
    private String tag;

    /**
     * 优惠券数量，如果是0，则是无限量
     */
    private Integer total;

    /**
     * 优惠金额，
     */
    private BigDecimal discount;

    /**
     * 最少消费金额才能使用优惠券。
     */
    private BigDecimal min;

    /**
     * 用户领券限制数量，如果是0，则是不限制；默认是1，限领一张.
     */
    private Short limit;

    /**
     * 优惠券赠送类型，如果是0则通用券，用户领取；如果是1，则是注册赠券；如果是2，则是优惠券码兑换；
     */
    private Short type;

    /**
     * 优惠券状态，如果是0则是正常可用；如果是1则是过期; 如果是2则是下架。
     */
    private Short status;

    /**
     * 商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。
     */
    private Short goodsType;

    /**
     * 商品限制值，goods_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。
     */
    private Integer[] goodsValue;

    /**
     * 优惠券兑换码
     */
    private String code;

    /**
     * 有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；
     */
    private Short timeType;

    /**
     * 基于领取时间的有效天数days。
     */
    private Short days;

    /**
     * 使用券开始时间
     */
    private LocalDateTime startTime;

    /**
     * 使用券截至时间
     */
    private LocalDateTime endTime;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * litemall_coupon
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 优惠券名称
     * @return name 优惠券名称
     */
    public String getName() {
        return name;
    }

    /**
     * 优惠券名称
     * @param name 优惠券名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 优惠券介绍，通常是显示优惠券使用限制文字
     * @return desc 优惠券介绍，通常是显示优惠券使用限制文字
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 优惠券介绍，通常是显示优惠券使用限制文字
     * @param desc 优惠券介绍，通常是显示优惠券使用限制文字
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 优惠券标签，例如新人专用
     * @return tag 优惠券标签，例如新人专用
     */
    public String getTag() {
        return tag;
    }

    /**
     * 优惠券标签，例如新人专用
     * @param tag 优惠券标签，例如新人专用
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * 优惠券数量，如果是0，则是无限量
     * @return total 优惠券数量，如果是0，则是无限量
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * 优惠券数量，如果是0，则是无限量
     * @param total 优惠券数量，如果是0，则是无限量
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * 优惠金额，
     * @return discount 优惠金额，
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * 优惠金额，
     * @param discount 优惠金额，
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * 最少消费金额才能使用优惠券。
     * @return min 最少消费金额才能使用优惠券。
     */
    public BigDecimal getMin() {
        return min;
    }

    /**
     * 最少消费金额才能使用优惠券。
     * @param min 最少消费金额才能使用优惠券。
     */
    public void setMin(BigDecimal min) {
        this.min = min;
    }

    /**
     * 用户领券限制数量，如果是0，则是不限制；默认是1，限领一张.
     * @return limit 用户领券限制数量，如果是0，则是不限制；默认是1，限领一张.
     */
    public Short getLimit() {
        return limit;
    }

    /**
     * 用户领券限制数量，如果是0，则是不限制；默认是1，限领一张.
     * @param limit 用户领券限制数量，如果是0，则是不限制；默认是1，限领一张.
     */
    public void setLimit(Short limit) {
        this.limit = limit;
    }

    /**
     * 优惠券赠送类型，如果是0则通用券，用户领取；如果是1，则是注册赠券；如果是2，则是优惠券码兑换；
     * @return type 优惠券赠送类型，如果是0则通用券，用户领取；如果是1，则是注册赠券；如果是2，则是优惠券码兑换；
     */
    public Short getType() {
        return type;
    }

    /**
     * 优惠券赠送类型，如果是0则通用券，用户领取；如果是1，则是注册赠券；如果是2，则是优惠券码兑换；
     * @param type 优惠券赠送类型，如果是0则通用券，用户领取；如果是1，则是注册赠券；如果是2，则是优惠券码兑换；
     */
    public void setType(Short type) {
        this.type = type;
    }

    /**
     * 优惠券状态，如果是0则是正常可用；如果是1则是过期; 如果是2则是下架。
     * @return status 优惠券状态，如果是0则是正常可用；如果是1则是过期; 如果是2则是下架。
     */
    public Short getStatus() {
        return status;
    }

    /**
     * 优惠券状态，如果是0则是正常可用；如果是1则是过期; 如果是2则是下架。
     * @param status 优惠券状态，如果是0则是正常可用；如果是1则是过期; 如果是2则是下架。
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * 商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。
     * @return goods_type 商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。
     */
    public Short getGoodsType() {
        return goodsType;
    }

    /**
     * 商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。
     * @param goodsType 商品限制类型，如果0则全商品，如果是1则是类目限制，如果是2则是商品限制。
     */
    public void setGoodsType(Short goodsType) {
        this.goodsType = goodsType;
    }

    /**
     * 商品限制值，goods_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。
     * @return goods_value 商品限制值，goods_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。
     */
    public Integer[] getGoodsValue() {
        return goodsValue;
    }

    /**
     * 商品限制值，goods_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。
     * @param goodsValue 商品限制值，goods_type如果是0则空集合，如果是1则是类目集合，如果是2则是商品集合。
     */
    public void setGoodsValue(Integer[] goodsValue) {
        this.goodsValue = goodsValue;
    }

    /**
     * 优惠券兑换码
     * @return code 优惠券兑换码
     */
    public String getCode() {
        return code;
    }

    /**
     * 优惠券兑换码
     * @param code 优惠券兑换码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；
     * @return time_type 有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；
     */
    public Short getTimeType() {
        return timeType;
    }

    /**
     * 有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；
     * @param timeType 有效时间限制，如果是0，则基于领取时间的有效天数days；如果是1，则start_time和end_time是优惠券有效期；
     */
    public void setTimeType(Short timeType) {
        this.timeType = timeType;
    }

    /**
     * 基于领取时间的有效天数days。
     * @return days 基于领取时间的有效天数days。
     */
    public Short getDays() {
        return days;
    }

    /**
     * 基于领取时间的有效天数days。
     * @param days 基于领取时间的有效天数days。
     */
    public void setDays(Short days) {
        this.days = days;
    }

    /**
     * 使用券开始时间
     * @return start_time 使用券开始时间
     */
    public LocalDateTime getStartTime() {
        return startTime;
    }

    /**
     * 使用券开始时间
     * @param startTime 使用券开始时间
     */
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    /**
     * 使用券截至时间
     * @return end_time 使用券截至时间
     */
    public LocalDateTime getEndTime() {
        return endTime;
    }

    /**
     * 使用券截至时间
     * @param endTime 使用券截至时间
     */
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 优惠券信息及规则表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 优惠券信息及规则表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", desc=").append(desc);
        sb.append(", tag=").append(tag);
        sb.append(", total=").append(total);
        sb.append(", discount=").append(discount);
        sb.append(", min=").append(min);
        sb.append(", limit=").append(limit);
        sb.append(", type=").append(type);
        sb.append(", status=").append(status);
        sb.append(", goodsType=").append(goodsType);
        sb.append(", goodsValue=").append(goodsValue);
        sb.append(", code=").append(code);
        sb.append(", timeType=").append(timeType);
        sb.append(", days=").append(days);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 优惠券信息及规则表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallCoupon other = (LitemallCoupon) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getDesc() == null ? other.getDesc() == null : this.getDesc().equals(other.getDesc()))
            && (this.getTag() == null ? other.getTag() == null : this.getTag().equals(other.getTag()))
            && (this.getTotal() == null ? other.getTotal() == null : this.getTotal().equals(other.getTotal()))
            && (this.getDiscount() == null ? other.getDiscount() == null : this.getDiscount().equals(other.getDiscount()))
            && (this.getMin() == null ? other.getMin() == null : this.getMin().equals(other.getMin()))
            && (this.getLimit() == null ? other.getLimit() == null : this.getLimit().equals(other.getLimit()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getGoodsType() == null ? other.getGoodsType() == null : this.getGoodsType().equals(other.getGoodsType()))
            && (Arrays.equals(this.getGoodsValue(), other.getGoodsValue()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getTimeType() == null ? other.getTimeType() == null : this.getTimeType().equals(other.getTimeType()))
            && (this.getDays() == null ? other.getDays() == null : this.getDays().equals(other.getDays()))
            && (this.getStartTime() == null ? other.getStartTime() == null : this.getStartTime().equals(other.getStartTime()))
            && (this.getEndTime() == null ? other.getEndTime() == null : this.getEndTime().equals(other.getEndTime()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()));
    }

    /**
     * 优惠券信息及规则表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getDesc() == null) ? 0 : getDesc().hashCode());
        result = prime * result + ((getTag() == null) ? 0 : getTag().hashCode());
        result = prime * result + ((getTotal() == null) ? 0 : getTotal().hashCode());
        result = prime * result + ((getDiscount() == null) ? 0 : getDiscount().hashCode());
        result = prime * result + ((getMin() == null) ? 0 : getMin().hashCode());
        result = prime * result + ((getLimit() == null) ? 0 : getLimit().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getGoodsType() == null) ? 0 : getGoodsType().hashCode());
        result = prime * result + (Arrays.hashCode(getGoodsValue()));
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getTimeType() == null) ? 0 : getTimeType().hashCode());
        result = prime * result + ((getDays() == null) ? 0 : getDays().hashCode());
        result = prime * result + ((getStartTime() == null) ? 0 : getStartTime().hashCode());
        result = prime * result + ((getEndTime() == null) ? 0 : getEndTime().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        return result;
    }

    /**
     * litemall_coupon
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_coupon
         */
        private final Boolean value;

        /**
         * litemall_coupon
         */
        private final String name;

        /**
         * 优惠券信息及规则表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 优惠券信息及规则表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 优惠券信息及规则表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 优惠券信息及规则表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 优惠券信息及规则表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 优惠券信息及规则表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_coupon
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        name("name", "name", "VARCHAR", true),
        desc("desc", "desc", "VARCHAR", true),
        tag("tag", "tag", "VARCHAR", false),
        total("total", "total", "INTEGER", false),
        discount("discount", "discount", "DECIMAL", false),
        min("min", "min", "DECIMAL", true),
        limit("limit", "limit", "SMALLINT", true),
        type("type", "type", "SMALLINT", true),
        status("status", "status", "SMALLINT", true),
        goodsType("goods_type", "goodsType", "SMALLINT", false),
        goodsValue("goods_value", "goodsValue", "VARCHAR", false),
        code("code", "code", "VARCHAR", false),
        timeType("time_type", "timeType", "SMALLINT", false),
        days("days", "days", "SMALLINT", true),
        startTime("start_time", "startTime", "TIMESTAMP", false),
        endTime("end_time", "endTime", "TIMESTAMP", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false);

        /**
         * litemall_coupon
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_coupon
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_coupon
         */
        private final String column;

        /**
         * litemall_coupon
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_coupon
         */
        private final String javaProperty;

        /**
         * litemall_coupon
         */
        private final String jdbcType;

        /**
         * 优惠券信息及规则表
         */
        public String value() {
            return this.column;
        }

        /**
         * 优惠券信息及规则表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 优惠券信息及规则表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 优惠券信息及规则表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 优惠券信息及规则表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 优惠券信息及规则表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 优惠券信息及规则表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 优惠券信息及规则表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 优惠券信息及规则表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 优惠券信息及规则表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 优惠券信息及规则表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}