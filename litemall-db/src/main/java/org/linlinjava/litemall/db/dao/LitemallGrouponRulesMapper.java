package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallGrouponRules;
import org.linlinjava.litemall.db.example.LitemallGrouponRulesExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallGrouponRulesMapper {
    /**
     * 团购规则表
     */
    long countByExample(LitemallGrouponRulesExample example);

    /**
     * 团购规则表
     */
    int deleteByExample(LitemallGrouponRulesExample example);

    /**
     * 团购规则表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 团购规则表
     */
    int insert(LitemallGrouponRules record);

    /**
     * 团购规则表
     */
    int insertSelective(LitemallGrouponRules record);

    /**
     * 团购规则表
     */
    LitemallGrouponRules selectOneByExample(LitemallGrouponRulesExample example);

    /**
     * 团购规则表
     */
    LitemallGrouponRules selectOneByExampleSelective(@Param("example") LitemallGrouponRulesExample example, @Param("selective") LitemallGrouponRules.Column ... selective);

    /**
     * 团购规则表
     */
    List<LitemallGrouponRules> selectByExampleSelective(@Param("example") LitemallGrouponRulesExample example, @Param("selective") LitemallGrouponRules.Column ... selective);

    /**
     * 团购规则表
     */
    List<LitemallGrouponRules> selectByExample(LitemallGrouponRulesExample example);

    /**
     * 团购规则表
     */
    LitemallGrouponRules selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallGrouponRules.Column ... selective);

    /**
     * 团购规则表
     */
    LitemallGrouponRules selectByPrimaryKey(Integer id);

    /**
     * 团购规则表
     */
    LitemallGrouponRules selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 团购规则表
     */
    int updateByExampleSelective(@Param("record") LitemallGrouponRules record, @Param("example") LitemallGrouponRulesExample example);

    /**
     * 团购规则表
     */
    int updateByExample(@Param("record") LitemallGrouponRules record, @Param("example") LitemallGrouponRulesExample example);

    /**
     * 团购规则表
     */
    int updateByPrimaryKeySelective(LitemallGrouponRules record);

    /**
     * 团购规则表
     */
    int updateByPrimaryKey(LitemallGrouponRules record);

    /**
     * 团购规则表
     */
    int logicalDeleteByExample(@Param("example") LitemallGrouponRulesExample example);

    /**
     * 团购规则表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}