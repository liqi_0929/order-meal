package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallBrand;
import org.linlinjava.litemall.db.example.LitemallBrandExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallBrandMapper {
    /**
     * 品牌商表
     */
    long countByExample(LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int deleteByExample(LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 品牌商表
     */
    int insert(LitemallBrand record);

    /**
     * 品牌商表
     */
    int insertSelective(LitemallBrand record);

    /**
     * 品牌商表
     */
    LitemallBrand selectOneByExample(LitemallBrandExample example);

    /**
     * 品牌商表
     */
    LitemallBrand selectOneByExampleSelective(@Param("example") LitemallBrandExample example, @Param("selective") LitemallBrand.Column ... selective);

    /**
     * 品牌商表
     */
    List<LitemallBrand> selectByExampleSelective(@Param("example") LitemallBrandExample example, @Param("selective") LitemallBrand.Column ... selective);

    /**
     * 品牌商表
     */
    List<LitemallBrand> selectByExample(LitemallBrandExample example);

    /**
     * 品牌商表
     */
    LitemallBrand selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallBrand.Column ... selective);

    /**
     * 品牌商表
     */
    LitemallBrand selectByPrimaryKey(Integer id);

    /**
     * 品牌商表
     */
    LitemallBrand selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 品牌商表
     */
    int updateByExampleSelective(@Param("record") LitemallBrand record, @Param("example") LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int updateByExample(@Param("record") LitemallBrand record, @Param("example") LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int updateByPrimaryKeySelective(LitemallBrand record);

    /**
     * 品牌商表
     */
    int updateByPrimaryKey(LitemallBrand record);

    /**
     * 品牌商表
     */
    int logicalDeleteByExample(@Param("example") LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}