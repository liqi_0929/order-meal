package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallQrcode;
import org.linlinjava.litemall.db.example.LitemallQrcodeExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallQrcodeMapper {
    /**
     * 
     */
    long countByExample(LitemallQrcodeExample example);

    /**
     * 
     */
    int deleteByExample(LitemallQrcodeExample example);

    /**
     * 
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 
     */
    int insert(LitemallQrcode record);

    /**
     * 
     */
    int insertSelective(LitemallQrcode record);

    /**
     * 
     */
    LitemallQrcode selectOneByExample(LitemallQrcodeExample example);

    /**
     * 
     */
    LitemallQrcode selectOneByExampleSelective(@Param("example") LitemallQrcodeExample example, @Param("selective") LitemallQrcode.Column ... selective);

    /**
     * 
     */
    LitemallQrcode selectOneByExampleWithBLOBs(LitemallQrcodeExample example);

    /**
     * 
     */
    List<LitemallQrcode> selectByExampleSelective(@Param("example") LitemallQrcodeExample example, @Param("selective") LitemallQrcode.Column ... selective);

    /**
     * 
     */
    List<LitemallQrcode> selectByExampleWithBLOBs(LitemallQrcodeExample example);

    /**
     * 
     */
    List<LitemallQrcode> selectByExample(LitemallQrcodeExample example);

    /**
     * 
     */
    LitemallQrcode selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallQrcode.Column ... selective);

    /**
     * 
     */
    LitemallQrcode selectByPrimaryKey(Integer id);

    /**
     * 
     */
    LitemallQrcode selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 
     */
    int updateByExampleSelective(@Param("record") LitemallQrcode record, @Param("example") LitemallQrcodeExample example);

    /**
     * 
     */
    int updateByExampleWithBLOBs(@Param("record") LitemallQrcode record, @Param("example") LitemallQrcodeExample example);

    /**
     * 
     */
    int updateByExample(@Param("record") LitemallQrcode record, @Param("example") LitemallQrcodeExample example);

    /**
     * 
     */
    int updateByPrimaryKeySelective(LitemallQrcode record);

    /**
     * 
     */
    int updateByPrimaryKeyWithBLOBs(LitemallQrcode record);

    /**
     * 
     */
    int updateByPrimaryKey(LitemallQrcode record);

    /**
     * 
     */
    int logicalDeleteByExample(@Param("example") LitemallQrcodeExample example);

    /**
     * 
     */
    int logicalDeleteByPrimaryKey(Integer id);
}