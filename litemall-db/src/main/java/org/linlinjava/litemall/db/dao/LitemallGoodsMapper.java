package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallGoods;
import org.linlinjava.litemall.db.example.LitemallGoodsExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallGoodsMapper {
    /**
     * 商品基本信息表
     */
    long countByExample(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int deleteByExample(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 商品基本信息表
     */
    int insert(LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int insertSelective(LitemallGoods record);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectOneByExample(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectOneByExampleSelective(@Param("example") LitemallGoodsExample example, @Param("selective") LitemallGoods.Column ... selective);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectOneByExampleWithBLOBs(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    List<LitemallGoods> selectByExampleSelective(@Param("example") LitemallGoodsExample example, @Param("selective") LitemallGoods.Column ... selective);

    /**
     * 商品基本信息表
     */
    List<LitemallGoods> selectByExampleWithBLOBs(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    List<LitemallGoods> selectByExample(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallGoods.Column ... selective);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectByPrimaryKey(Integer id);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 商品基本信息表
     */
    int updateByExampleSelective(@Param("record") LitemallGoods record, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int updateByExampleWithBLOBs(@Param("record") LitemallGoods record, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int updateByExample(@Param("record") LitemallGoods record, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int updateByPrimaryKeySelective(LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int updateByPrimaryKeyWithBLOBs(LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int updateByPrimaryKey(LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int logicalDeleteByExample(@Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}