package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_goods
 * Database Table Remarks : 
 *   商品基本信息表
 * @author linlinjava
 */
public class LitemallGoods implements Serializable {
    /**
     * litemall_goods
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_goods
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    private Boolean add;

    public Boolean getAdd() {
        return add;
    }

    public void setAdd(Boolean add) {
        this.add = add;
    }

    /**
     * 商品编号
     */
    private String goodsSn;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品所属类目ID
     */
    private Integer categoryId;

    /**
     * 
     */
    private Integer brandId;

    /**
     * 商品宣传图片列表，采用JSON数组格式
     */
    private String[] gallery;

    /**
     * 商品关键字，采用逗号间隔
     */
    private String keywords;

    /**
     * 商品简介
     */
    private String brief;

    /**
     * 是否上架
     */
    private Boolean isOnSale;

    /**
     * 
     */
    private Short sortOrder;

    /**
     * 商品页面商品图片
     */
    private String picUrl;

    /**
     * 商品分享海报
     */
    private String shareUrl;

    /**
     * 是否新品首发，如果设置则可以在新品首发页面展示
     */
    private Boolean isNew;

    /**
     * 是否人气推荐，如果设置则可以在人气推荐页面展示
     */
    private Boolean isHot;

    /**
     * 商品单位，例如件、盒
     */
    private String unit;

    /**
     * 专柜价格
     */
    private BigDecimal counterPrice;

    /**
     * 零售价格
     */
    private BigDecimal retailPrice;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 商品详细介绍，是富文本格式
     */
    private String detail;

    /**
     * litemall_goods
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 商品编号
     * @return goods_sn 商品编号
     */
    public String getGoodsSn() {
        return goodsSn;
    }

    /**
     * 商品编号
     * @param goodsSn 商品编号
     */
    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn;
    }

    /**
     * 商品名称
     * @return name 商品名称
     */
    public String getName() {
        return name;
    }

    /**
     * 商品名称
     * @param name 商品名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 商品所属类目ID
     * @return category_id 商品所属类目ID
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * 商品所属类目ID
     * @param categoryId 商品所属类目ID
     */
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * 
     * @return brand_id 
     */
    public Integer getBrandId() {
        return brandId;
    }

    /**
     * 
     * @param brandId 
     */
    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    /**
     * 商品宣传图片列表，采用JSON数组格式
     * @return gallery 商品宣传图片列表，采用JSON数组格式
     */
    public String[] getGallery() {
        return gallery;
    }

    /**
     * 商品宣传图片列表，采用JSON数组格式
     * @param gallery 商品宣传图片列表，采用JSON数组格式
     */
    public void setGallery(String[] gallery) {
        this.gallery = gallery;
    }

    /**
     * 商品关键字，采用逗号间隔
     * @return keywords 商品关键字，采用逗号间隔
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * 商品关键字，采用逗号间隔
     * @param keywords 商品关键字，采用逗号间隔
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    /**
     * 商品简介
     * @return brief 商品简介
     */
    public String getBrief() {
        return brief;
    }

    /**
     * 商品简介
     * @param brief 商品简介
     */
    public void setBrief(String brief) {
        this.brief = brief;
    }

    /**
     * 是否上架
     * @return is_on_sale 是否上架
     */
    public Boolean getIsOnSale() {
        return isOnSale;
    }

    /**
     * 是否上架
     * @param isOnSale 是否上架
     */
    public void setIsOnSale(Boolean isOnSale) {
        this.isOnSale = isOnSale;
    }

    /**
     * 
     * @return sort_order 
     */
    public Short getSortOrder() {
        return sortOrder;
    }

    /**
     * 
     * @param sortOrder 
     */
    public void setSortOrder(Short sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * 商品页面商品图片
     * @return pic_url 商品页面商品图片
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 商品页面商品图片
     * @param picUrl 商品页面商品图片
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * 商品分享海报
     * @return share_url 商品分享海报
     */
    public String getShareUrl() {
        return shareUrl;
    }

    /**
     * 商品分享海报
     * @param shareUrl 商品分享海报
     */
    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    /**
     * 是否新品首发，如果设置则可以在新品首发页面展示
     * @return is_new 是否新品首发，如果设置则可以在新品首发页面展示
     */
    public Boolean getIsNew() {
        return isNew;
    }

    /**
     * 是否新品首发，如果设置则可以在新品首发页面展示
     * @param isNew 是否新品首发，如果设置则可以在新品首发页面展示
     */
    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    /**
     * 是否人气推荐，如果设置则可以在人气推荐页面展示
     * @return is_hot 是否人气推荐，如果设置则可以在人气推荐页面展示
     */
    public Boolean getIsHot() {
        return isHot;
    }

    /**
     * 是否人气推荐，如果设置则可以在人气推荐页面展示
     * @param isHot 是否人气推荐，如果设置则可以在人气推荐页面展示
     */
    public void setIsHot(Boolean isHot) {
        this.isHot = isHot;
    }

    /**
     * 商品单位，例如件、盒
     * @return unit 商品单位，例如件、盒
     */
    public String getUnit() {
        return unit;
    }

    /**
     * 商品单位，例如件、盒
     * @param unit 商品单位，例如件、盒
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * 专柜价格
     * @return counter_price 专柜价格
     */
    public BigDecimal getCounterPrice() {
        return counterPrice;
    }

    /**
     * 专柜价格
     * @param counterPrice 专柜价格
     */
    public void setCounterPrice(BigDecimal counterPrice) {
        this.counterPrice = counterPrice;
    }

    /**
     * 零售价格
     * @return retail_price 零售价格
     */
    public BigDecimal getRetailPrice() {
        return retailPrice;
    }

    /**
     * 零售价格
     * @param retailPrice 零售价格
     */
    public void setRetailPrice(BigDecimal retailPrice) {
        this.retailPrice = retailPrice;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 商品基本信息表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 商品详细介绍，是富文本格式
     * @return detail 商品详细介绍，是富文本格式
     */
    public String getDetail() {
        return detail;
    }

    /**
     * 商品详细介绍，是富文本格式
     * @param detail 商品详细介绍，是富文本格式
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * 商品基本信息表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", goodsSn=").append(goodsSn);
        sb.append(", name=").append(name);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", brandId=").append(brandId);
        sb.append(", gallery=").append(gallery);
        sb.append(", keywords=").append(keywords);
        sb.append(", brief=").append(brief);
        sb.append(", isOnSale=").append(isOnSale);
        sb.append(", sortOrder=").append(sortOrder);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", shareUrl=").append(shareUrl);
        sb.append(", isNew=").append(isNew);
        sb.append(", isHot=").append(isHot);
        sb.append(", unit=").append(unit);
        sb.append(", counterPrice=").append(counterPrice);
        sb.append(", retailPrice=").append(retailPrice);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", detail=").append(detail);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 商品基本信息表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallGoods other = (LitemallGoods) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getGoodsSn() == null ? other.getGoodsSn() == null : this.getGoodsSn().equals(other.getGoodsSn()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getCategoryId() == null ? other.getCategoryId() == null : this.getCategoryId().equals(other.getCategoryId()))
            && (this.getBrandId() == null ? other.getBrandId() == null : this.getBrandId().equals(other.getBrandId()))
            && (Arrays.equals(this.getGallery(), other.getGallery()))
            && (this.getKeywords() == null ? other.getKeywords() == null : this.getKeywords().equals(other.getKeywords()))
            && (this.getBrief() == null ? other.getBrief() == null : this.getBrief().equals(other.getBrief()))
            && (this.getIsOnSale() == null ? other.getIsOnSale() == null : this.getIsOnSale().equals(other.getIsOnSale()))
            && (this.getSortOrder() == null ? other.getSortOrder() == null : this.getSortOrder().equals(other.getSortOrder()))
            && (this.getPicUrl() == null ? other.getPicUrl() == null : this.getPicUrl().equals(other.getPicUrl()))
            && (this.getShareUrl() == null ? other.getShareUrl() == null : this.getShareUrl().equals(other.getShareUrl()))
            && (this.getIsNew() == null ? other.getIsNew() == null : this.getIsNew().equals(other.getIsNew()))
            && (this.getIsHot() == null ? other.getIsHot() == null : this.getIsHot().equals(other.getIsHot()))
            && (this.getUnit() == null ? other.getUnit() == null : this.getUnit().equals(other.getUnit()))
            && (this.getCounterPrice() == null ? other.getCounterPrice() == null : this.getCounterPrice().equals(other.getCounterPrice()))
            && (this.getRetailPrice() == null ? other.getRetailPrice() == null : this.getRetailPrice().equals(other.getRetailPrice()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getDetail() == null ? other.getDetail() == null : this.getDetail().equals(other.getDetail()));
    }

    /**
     * 商品基本信息表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getGoodsSn() == null) ? 0 : getGoodsSn().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getCategoryId() == null) ? 0 : getCategoryId().hashCode());
        result = prime * result + ((getBrandId() == null) ? 0 : getBrandId().hashCode());
        result = prime * result + (Arrays.hashCode(getGallery()));
        result = prime * result + ((getKeywords() == null) ? 0 : getKeywords().hashCode());
        result = prime * result + ((getBrief() == null) ? 0 : getBrief().hashCode());
        result = prime * result + ((getIsOnSale() == null) ? 0 : getIsOnSale().hashCode());
        result = prime * result + ((getSortOrder() == null) ? 0 : getSortOrder().hashCode());
        result = prime * result + ((getPicUrl() == null) ? 0 : getPicUrl().hashCode());
        result = prime * result + ((getShareUrl() == null) ? 0 : getShareUrl().hashCode());
        result = prime * result + ((getIsNew() == null) ? 0 : getIsNew().hashCode());
        result = prime * result + ((getIsHot() == null) ? 0 : getIsHot().hashCode());
        result = prime * result + ((getUnit() == null) ? 0 : getUnit().hashCode());
        result = prime * result + ((getCounterPrice() == null) ? 0 : getCounterPrice().hashCode());
        result = prime * result + ((getRetailPrice() == null) ? 0 : getRetailPrice().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getDetail() == null) ? 0 : getDetail().hashCode());
        return result;
    }

    /**
     * litemall_goods
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_goods
         */
        private final Boolean value;

        /**
         * litemall_goods
         */
        private final String name;

        /**
         * 商品基本信息表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 商品基本信息表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 商品基本信息表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 商品基本信息表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 商品基本信息表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 商品基本信息表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_goods
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        goodsSn("goods_sn", "goodsSn", "VARCHAR", false),
        name("name", "name", "VARCHAR", true),
        categoryId("category_id", "categoryId", "INTEGER", false),
        brandId("brand_id", "brandId", "INTEGER", false),
        gallery("gallery", "gallery", "VARCHAR", false),
        keywords("keywords", "keywords", "VARCHAR", false),
        brief("brief", "brief", "VARCHAR", false),
        isOnSale("is_on_sale", "isOnSale", "BIT", false),
        sortOrder("sort_order", "sortOrder", "SMALLINT", false),
        picUrl("pic_url", "picUrl", "VARCHAR", false),
        shareUrl("share_url", "shareUrl", "VARCHAR", false),
        isNew("is_new", "isNew", "BIT", false),
        isHot("is_hot", "isHot", "BIT", false),
        unit("unit", "unit", "VARCHAR", false),
        counterPrice("counter_price", "counterPrice", "DECIMAL", false),
        retailPrice("retail_price", "retailPrice", "DECIMAL", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        detail("detail", "detail", "LONGVARCHAR", false);

        /**
         * litemall_goods
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_goods
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_goods
         */
        private final String column;

        /**
         * litemall_goods
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_goods
         */
        private final String javaProperty;

        /**
         * litemall_goods
         */
        private final String jdbcType;

        /**
         * 商品基本信息表
         */
        public String value() {
            return this.column;
        }

        /**
         * 商品基本信息表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 商品基本信息表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 商品基本信息表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 商品基本信息表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 商品基本信息表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 商品基本信息表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 商品基本信息表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 商品基本信息表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 商品基本信息表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 商品基本信息表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}