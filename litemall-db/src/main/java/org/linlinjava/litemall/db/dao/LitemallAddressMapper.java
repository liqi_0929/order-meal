package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallAddress;
import org.linlinjava.litemall.db.example.LitemallAddressExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallAddressMapper {
    /**
     * 收货地址表
     */
    long countByExample(LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int deleteByExample(LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 收货地址表
     */
    int insert(LitemallAddress record);

    /**
     * 收货地址表
     */
    int insertSelective(LitemallAddress record);

    /**
     * 收货地址表
     */
    LitemallAddress selectOneByExample(LitemallAddressExample example);

    /**
     * 收货地址表
     */
    LitemallAddress selectOneByExampleSelective(@Param("example") LitemallAddressExample example, @Param("selective") LitemallAddress.Column ... selective);

    /**
     * 收货地址表
     */
    List<LitemallAddress> selectByExampleSelective(@Param("example") LitemallAddressExample example, @Param("selective") LitemallAddress.Column ... selective);

    /**
     * 收货地址表
     */
    List<LitemallAddress> selectByExample(LitemallAddressExample example);

    /**
     * 收货地址表
     */
    LitemallAddress selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallAddress.Column ... selective);

    /**
     * 收货地址表
     */
    LitemallAddress selectByPrimaryKey(Integer id);

    /**
     * 收货地址表
     */
    LitemallAddress selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 收货地址表
     */
    int updateByExampleSelective(@Param("record") LitemallAddress record, @Param("example") LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int updateByExample(@Param("record") LitemallAddress record, @Param("example") LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int updateByPrimaryKeySelective(LitemallAddress record);

    /**
     * 收货地址表
     */
    int updateByPrimaryKey(LitemallAddress record);

    /**
     * 收货地址表
     */
    int logicalDeleteByExample(@Param("example") LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}