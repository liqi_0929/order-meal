package org.linlinjava.litemall.db.vo;

import org.linlinjava.litemall.db.domain.LitemallGoodsSpecification;

import java.io.Serializable;
import java.util.List;

public class GoodsSpecificationVo implements Serializable {

    private String name;
    private List<LitemallGoodsSpecification> valueList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LitemallGoodsSpecification> getValueList() {
        return valueList;
    }

    public void setValueList(List<LitemallGoodsSpecification> valueList) {
        this.valueList = valueList;
    }
}
