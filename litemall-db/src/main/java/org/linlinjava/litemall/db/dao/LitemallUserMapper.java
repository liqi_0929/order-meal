package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallUser;
import org.linlinjava.litemall.db.example.LitemallUserExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallUserMapper {
    /**
     * 用户表
     */
    long countByExample(LitemallUserExample example);

    /**
     * 用户表
     */
    int deleteByExample(LitemallUserExample example);

    /**
     * 用户表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 用户表
     */
    int insert(LitemallUser record);

    /**
     * 用户表
     */
    int insertSelective(LitemallUser record);

    /**
     * 用户表
     */
    LitemallUser selectOneByExample(LitemallUserExample example);

    /**
     * 用户表
     */
    LitemallUser selectOneByExampleSelective(@Param("example") LitemallUserExample example, @Param("selective") LitemallUser.Column ... selective);

    /**
     * 用户表
     */
    List<LitemallUser> selectByExampleSelective(@Param("example") LitemallUserExample example, @Param("selective") LitemallUser.Column ... selective);

    /**
     * 用户表
     */
    List<LitemallUser> selectByExample(LitemallUserExample example);

    /**
     * 用户表
     */
    LitemallUser selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallUser.Column ... selective);

    /**
     * 用户表
     */
    LitemallUser selectByPrimaryKey(Integer id);

    /**
     * 用户表
     */
    LitemallUser selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 用户表
     */
    int updateByExampleSelective(@Param("record") LitemallUser record, @Param("example") LitemallUserExample example);

    /**
     * 用户表
     */
    int updateByExample(@Param("record") LitemallUser record, @Param("example") LitemallUserExample example);

    /**
     * 用户表
     */
    int updateByPrimaryKeySelective(LitemallUser record);

    /**
     * 用户表
     */
    int updateByPrimaryKey(LitemallUser record);

    /**
     * 用户表
     */
    int logicalDeleteByExample(@Param("example") LitemallUserExample example);

    /**
     * 用户表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}