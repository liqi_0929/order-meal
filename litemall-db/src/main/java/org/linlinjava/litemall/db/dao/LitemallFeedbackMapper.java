package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallFeedback;
import org.linlinjava.litemall.db.example.LitemallFeedbackExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallFeedbackMapper {
    /**
     * 意见反馈表
     */
    long countByExample(LitemallFeedbackExample example);

    /**
     * 意见反馈表
     */
    int deleteByExample(LitemallFeedbackExample example);

    /**
     * 意见反馈表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 意见反馈表
     */
    int insert(LitemallFeedback record);

    /**
     * 意见反馈表
     */
    int insertSelective(LitemallFeedback record);

    /**
     * 意见反馈表
     */
    LitemallFeedback selectOneByExample(LitemallFeedbackExample example);

    /**
     * 意见反馈表
     */
    LitemallFeedback selectOneByExampleSelective(@Param("example") LitemallFeedbackExample example, @Param("selective") LitemallFeedback.Column ... selective);

    /**
     * 意见反馈表
     */
    List<LitemallFeedback> selectByExampleSelective(@Param("example") LitemallFeedbackExample example, @Param("selective") LitemallFeedback.Column ... selective);

    /**
     * 意见反馈表
     */
    List<LitemallFeedback> selectByExample(LitemallFeedbackExample example);

    /**
     * 意见反馈表
     */
    LitemallFeedback selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallFeedback.Column ... selective);

    /**
     * 意见反馈表
     */
    LitemallFeedback selectByPrimaryKey(Integer id);

    /**
     * 意见反馈表
     */
    LitemallFeedback selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 意见反馈表
     */
    int updateByExampleSelective(@Param("record") LitemallFeedback record, @Param("example") LitemallFeedbackExample example);

    /**
     * 意见反馈表
     */
    int updateByExample(@Param("record") LitemallFeedback record, @Param("example") LitemallFeedbackExample example);

    /**
     * 意见反馈表
     */
    int updateByPrimaryKeySelective(LitemallFeedback record);

    /**
     * 意见反馈表
     */
    int updateByPrimaryKey(LitemallFeedback record);

    /**
     * 意见反馈表
     */
    int logicalDeleteByExample(@Param("example") LitemallFeedbackExample example);

    /**
     * 意见反馈表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}