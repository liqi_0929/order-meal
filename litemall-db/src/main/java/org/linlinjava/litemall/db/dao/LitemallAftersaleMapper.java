package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallAftersale;
import org.linlinjava.litemall.db.example.LitemallAftersaleExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallAftersaleMapper {
    /**
     * 售后表
     */
    long countByExample(LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int deleteByExample(LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 售后表
     */
    int insert(LitemallAftersale record);

    /**
     * 售后表
     */
    int insertSelective(LitemallAftersale record);

    /**
     * 售后表
     */
    LitemallAftersale selectOneByExample(LitemallAftersaleExample example);

    /**
     * 售后表
     */
    LitemallAftersale selectOneByExampleSelective(@Param("example") LitemallAftersaleExample example, @Param("selective") LitemallAftersale.Column ... selective);

    /**
     * 售后表
     */
    List<LitemallAftersale> selectByExampleSelective(@Param("example") LitemallAftersaleExample example, @Param("selective") LitemallAftersale.Column ... selective);

    /**
     * 售后表
     */
    List<LitemallAftersale> selectByExample(LitemallAftersaleExample example);

    /**
     * 售后表
     */
    LitemallAftersale selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallAftersale.Column ... selective);

    /**
     * 售后表
     */
    LitemallAftersale selectByPrimaryKey(Integer id);

    /**
     * 售后表
     */
    LitemallAftersale selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 售后表
     */
    int updateByExampleSelective(@Param("record") LitemallAftersale record, @Param("example") LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int updateByExample(@Param("record") LitemallAftersale record, @Param("example") LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int updateByPrimaryKeySelective(LitemallAftersale record);

    /**
     * 售后表
     */
    int updateByPrimaryKey(LitemallAftersale record);

    /**
     * 售后表
     */
    int logicalDeleteByExample(@Param("example") LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}