package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallGoodsSpecification;
import org.linlinjava.litemall.db.example.LitemallGoodsSpecificationExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallGoodsSpecificationMapper {
    /**
     * 商品规格表
     */
    long countByExample(LitemallGoodsSpecificationExample example);

    /**
     * 商品规格表
     */
    int deleteByExample(LitemallGoodsSpecificationExample example);

    /**
     * 商品规格表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 商品规格表
     */
    int insert(LitemallGoodsSpecification record);

    /**
     * 商品规格表
     */
    int insertSelective(LitemallGoodsSpecification record);

    /**
     * 商品规格表
     */
    LitemallGoodsSpecification selectOneByExample(LitemallGoodsSpecificationExample example);

    /**
     * 商品规格表
     */
    LitemallGoodsSpecification selectOneByExampleSelective(@Param("example") LitemallGoodsSpecificationExample example, @Param("selective") LitemallGoodsSpecification.Column ... selective);

    /**
     * 商品规格表
     */
    List<LitemallGoodsSpecification> selectByExampleSelective(@Param("example") LitemallGoodsSpecificationExample example, @Param("selective") LitemallGoodsSpecification.Column ... selective);

    /**
     * 商品规格表
     */
    List<LitemallGoodsSpecification> selectByExample(LitemallGoodsSpecificationExample example);

    /**
     * 商品规格表
     */
    LitemallGoodsSpecification selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallGoodsSpecification.Column ... selective);

    /**
     * 商品规格表
     */
    LitemallGoodsSpecification selectByPrimaryKey(Integer id);

    /**
     * 商品规格表
     */
    LitemallGoodsSpecification selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 商品规格表
     */
    int updateByExampleSelective(@Param("record") LitemallGoodsSpecification record, @Param("example") LitemallGoodsSpecificationExample example);

    /**
     * 商品规格表
     */
    int updateByExample(@Param("record") LitemallGoodsSpecification record, @Param("example") LitemallGoodsSpecificationExample example);

    /**
     * 商品规格表
     */
    int updateByPrimaryKeySelective(LitemallGoodsSpecification record);

    /**
     * 商品规格表
     */
    int updateByPrimaryKey(LitemallGoodsSpecification record);

    /**
     * 商品规格表
     */
    int logicalDeleteByExample(@Param("example") LitemallGoodsSpecificationExample example);

    /**
     * 商品规格表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}