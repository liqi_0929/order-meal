package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallGroupon;
import org.linlinjava.litemall.db.example.LitemallGrouponExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallGrouponMapper {
    /**
     * 团购活动表
     */
    long countByExample(LitemallGrouponExample example);

    /**
     * 团购活动表
     */
    int deleteByExample(LitemallGrouponExample example);

    /**
     * 团购活动表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 团购活动表
     */
    int insert(LitemallGroupon record);

    /**
     * 团购活动表
     */
    int insertSelective(LitemallGroupon record);

    /**
     * 团购活动表
     */
    LitemallGroupon selectOneByExample(LitemallGrouponExample example);

    /**
     * 团购活动表
     */
    LitemallGroupon selectOneByExampleSelective(@Param("example") LitemallGrouponExample example, @Param("selective") LitemallGroupon.Column ... selective);

    /**
     * 团购活动表
     */
    List<LitemallGroupon> selectByExampleSelective(@Param("example") LitemallGrouponExample example, @Param("selective") LitemallGroupon.Column ... selective);

    /**
     * 团购活动表
     */
    List<LitemallGroupon> selectByExample(LitemallGrouponExample example);

    /**
     * 团购活动表
     */
    LitemallGroupon selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallGroupon.Column ... selective);

    /**
     * 团购活动表
     */
    LitemallGroupon selectByPrimaryKey(Integer id);

    /**
     * 团购活动表
     */
    LitemallGroupon selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 团购活动表
     */
    int updateByExampleSelective(@Param("record") LitemallGroupon record, @Param("example") LitemallGrouponExample example);

    /**
     * 团购活动表
     */
    int updateByExample(@Param("record") LitemallGroupon record, @Param("example") LitemallGrouponExample example);

    /**
     * 团购活动表
     */
    int updateByPrimaryKeySelective(LitemallGroupon record);

    /**
     * 团购活动表
     */
    int updateByPrimaryKey(LitemallGroupon record);

    /**
     * 团购活动表
     */
    int logicalDeleteByExample(@Param("example") LitemallGrouponExample example);

    /**
     * 团购活动表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}