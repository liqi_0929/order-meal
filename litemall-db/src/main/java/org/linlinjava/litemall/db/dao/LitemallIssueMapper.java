package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallIssue;
import org.linlinjava.litemall.db.example.LitemallIssueExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallIssueMapper {
    /**
     * 常见问题表
     */
    long countByExample(LitemallIssueExample example);

    /**
     * 常见问题表
     */
    int deleteByExample(LitemallIssueExample example);

    /**
     * 常见问题表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 常见问题表
     */
    int insert(LitemallIssue record);

    /**
     * 常见问题表
     */
    int insertSelective(LitemallIssue record);

    /**
     * 常见问题表
     */
    LitemallIssue selectOneByExample(LitemallIssueExample example);

    /**
     * 常见问题表
     */
    LitemallIssue selectOneByExampleSelective(@Param("example") LitemallIssueExample example, @Param("selective") LitemallIssue.Column ... selective);

    /**
     * 常见问题表
     */
    List<LitemallIssue> selectByExampleSelective(@Param("example") LitemallIssueExample example, @Param("selective") LitemallIssue.Column ... selective);

    /**
     * 常见问题表
     */
    List<LitemallIssue> selectByExample(LitemallIssueExample example);

    /**
     * 常见问题表
     */
    LitemallIssue selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallIssue.Column ... selective);

    /**
     * 常见问题表
     */
    LitemallIssue selectByPrimaryKey(Integer id);

    /**
     * 常见问题表
     */
    LitemallIssue selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 常见问题表
     */
    int updateByExampleSelective(@Param("record") LitemallIssue record, @Param("example") LitemallIssueExample example);

    /**
     * 常见问题表
     */
    int updateByExample(@Param("record") LitemallIssue record, @Param("example") LitemallIssueExample example);

    /**
     * 常见问题表
     */
    int updateByPrimaryKeySelective(LitemallIssue record);

    /**
     * 常见问题表
     */
    int updateByPrimaryKey(LitemallIssue record);

    /**
     * 常见问题表
     */
    int logicalDeleteByExample(@Param("example") LitemallIssueExample example);

    /**
     * 常见问题表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}