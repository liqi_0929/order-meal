package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_groupon
 * Database Table Remarks : 
 *   团购活动表
 * @author linlinjava
 */
public class LitemallGroupon implements Serializable {
    /**
     * litemall_groupon
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_groupon
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    /**
     * 关联的订单ID
     */
    private Integer orderId;

    /**
     * 如果是开团用户，则groupon_id是0；如果是参团用户，则groupon_id是团购活动ID
     */
    private Integer grouponId;

    /**
     * 团购规则ID，关联litemall_groupon_rules表ID字段
     */
    private Integer rulesId;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 团购分享图片地址
     */
    private String shareUrl;

    /**
     * 开团用户ID
     */
    private Integer creatorUserId;

    /**
     * 开团时间
     */
    private LocalDateTime creatorUserTime;

    /**
     * 团购活动状态，开团未支付则0，开团中则1，开团失败则2
     */
    private Short status;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * litemall_groupon
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 关联的订单ID
     * @return order_id 关联的订单ID
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 关联的订单ID
     * @param orderId 关联的订单ID
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 如果是开团用户，则groupon_id是0；如果是参团用户，则groupon_id是团购活动ID
     * @return groupon_id 如果是开团用户，则groupon_id是0；如果是参团用户，则groupon_id是团购活动ID
     */
    public Integer getGrouponId() {
        return grouponId;
    }

    /**
     * 如果是开团用户，则groupon_id是0；如果是参团用户，则groupon_id是团购活动ID
     * @param grouponId 如果是开团用户，则groupon_id是0；如果是参团用户，则groupon_id是团购活动ID
     */
    public void setGrouponId(Integer grouponId) {
        this.grouponId = grouponId;
    }

    /**
     * 团购规则ID，关联litemall_groupon_rules表ID字段
     * @return rules_id 团购规则ID，关联litemall_groupon_rules表ID字段
     */
    public Integer getRulesId() {
        return rulesId;
    }

    /**
     * 团购规则ID，关联litemall_groupon_rules表ID字段
     * @param rulesId 团购规则ID，关联litemall_groupon_rules表ID字段
     */
    public void setRulesId(Integer rulesId) {
        this.rulesId = rulesId;
    }

    /**
     * 用户ID
     * @return user_id 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户ID
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 团购分享图片地址
     * @return share_url 团购分享图片地址
     */
    public String getShareUrl() {
        return shareUrl;
    }

    /**
     * 团购分享图片地址
     * @param shareUrl 团购分享图片地址
     */
    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    /**
     * 开团用户ID
     * @return creator_user_id 开团用户ID
     */
    public Integer getCreatorUserId() {
        return creatorUserId;
    }

    /**
     * 开团用户ID
     * @param creatorUserId 开团用户ID
     */
    public void setCreatorUserId(Integer creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    /**
     * 开团时间
     * @return creator_user_time 开团时间
     */
    public LocalDateTime getCreatorUserTime() {
        return creatorUserTime;
    }

    /**
     * 开团时间
     * @param creatorUserTime 开团时间
     */
    public void setCreatorUserTime(LocalDateTime creatorUserTime) {
        this.creatorUserTime = creatorUserTime;
    }

    /**
     * 团购活动状态，开团未支付则0，开团中则1，开团失败则2
     * @return status 团购活动状态，开团未支付则0，开团中则1，开团失败则2
     */
    public Short getStatus() {
        return status;
    }

    /**
     * 团购活动状态，开团未支付则0，开团中则1，开团失败则2
     * @param status 团购活动状态，开团未支付则0，开团中则1，开团失败则2
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 团购活动表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 团购活动表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", grouponId=").append(grouponId);
        sb.append(", rulesId=").append(rulesId);
        sb.append(", userId=").append(userId);
        sb.append(", shareUrl=").append(shareUrl);
        sb.append(", creatorUserId=").append(creatorUserId);
        sb.append(", creatorUserTime=").append(creatorUserTime);
        sb.append(", status=").append(status);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 团购活动表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallGroupon other = (LitemallGroupon) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getGrouponId() == null ? other.getGrouponId() == null : this.getGrouponId().equals(other.getGrouponId()))
            && (this.getRulesId() == null ? other.getRulesId() == null : this.getRulesId().equals(other.getRulesId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getShareUrl() == null ? other.getShareUrl() == null : this.getShareUrl().equals(other.getShareUrl()))
            && (this.getCreatorUserId() == null ? other.getCreatorUserId() == null : this.getCreatorUserId().equals(other.getCreatorUserId()))
            && (this.getCreatorUserTime() == null ? other.getCreatorUserTime() == null : this.getCreatorUserTime().equals(other.getCreatorUserTime()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()));
    }

    /**
     * 团购活动表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getGrouponId() == null) ? 0 : getGrouponId().hashCode());
        result = prime * result + ((getRulesId() == null) ? 0 : getRulesId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getShareUrl() == null) ? 0 : getShareUrl().hashCode());
        result = prime * result + ((getCreatorUserId() == null) ? 0 : getCreatorUserId().hashCode());
        result = prime * result + ((getCreatorUserTime() == null) ? 0 : getCreatorUserTime().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        return result;
    }

    /**
     * litemall_groupon
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_groupon
         */
        private final Boolean value;

        /**
         * litemall_groupon
         */
        private final String name;

        /**
         * 团购活动表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 团购活动表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 团购活动表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 团购活动表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 团购活动表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 团购活动表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_groupon
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        orderId("order_id", "orderId", "INTEGER", false),
        grouponId("groupon_id", "grouponId", "INTEGER", false),
        rulesId("rules_id", "rulesId", "INTEGER", false),
        userId("user_id", "userId", "INTEGER", false),
        shareUrl("share_url", "shareUrl", "VARCHAR", false),
        creatorUserId("creator_user_id", "creatorUserId", "INTEGER", false),
        creatorUserTime("creator_user_time", "creatorUserTime", "TIMESTAMP", false),
        status("status", "status", "SMALLINT", true),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false);

        /**
         * litemall_groupon
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_groupon
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_groupon
         */
        private final String column;

        /**
         * litemall_groupon
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_groupon
         */
        private final String javaProperty;

        /**
         * litemall_groupon
         */
        private final String jdbcType;

        /**
         * 团购活动表
         */
        public String value() {
            return this.column;
        }

        /**
         * 团购活动表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 团购活动表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 团购活动表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 团购活动表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 团购活动表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 团购活动表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 团购活动表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 团购活动表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 团购活动表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 团购活动表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}