package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_order_goods
 * Database Table Remarks : 
 *   订单商品表
 * @author linlinjava
 */
public class LitemallOrderGoods implements Serializable {
    /**
     * litemall_order_goods
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_order_goods
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    /**
     * 订单表的订单ID
     */
    private Integer orderId;

    /**
     * 商品表的商品ID
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品编号
     */
    private String goodsSn;

    /**
     * 商品货品表的货品ID
     */
    private Integer productId;

    /**
     * 商品货品的购买数量
     */
    private Short number;

    /**
     * 商品货品的售价
     */
    private BigDecimal price;

    /**
     * 商品货品的规格列表
     */
    private String[] specifications;

    /**
     * 商品货品图片或者商品图片
     */
    private String picUrl;

    /**
     * 订单商品评论，如果是-1，则超期不能评价；如果是0，则可以评价；如果是1，则追加评价；如果其他值，则是comment表里面的评论ID。
     */
    private Integer comment;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * litemall_order_goods
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 订单表的订单ID
     * @return order_id 订单表的订单ID
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 订单表的订单ID
     * @param orderId 订单表的订单ID
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 商品表的商品ID
     * @return goods_id 商品表的商品ID
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 商品表的商品ID
     * @param goodsId 商品表的商品ID
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 商品名称
     * @return goods_name 商品名称
     */
    public String getGoodsName() {
        return goodsName;
    }

    /**
     * 商品名称
     * @param goodsName 商品名称
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * 商品编号
     * @return goods_sn 商品编号
     */
    public String getGoodsSn() {
        return goodsSn;
    }

    /**
     * 商品编号
     * @param goodsSn 商品编号
     */
    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn;
    }

    /**
     * 商品货品表的货品ID
     * @return product_id 商品货品表的货品ID
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * 商品货品表的货品ID
     * @param productId 商品货品表的货品ID
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * 商品货品的购买数量
     * @return number 商品货品的购买数量
     */
    public Short getNumber() {
        return number;
    }

    /**
     * 商品货品的购买数量
     * @param number 商品货品的购买数量
     */
    public void setNumber(Short number) {
        this.number = number;
    }

    /**
     * 商品货品的售价
     * @return price 商品货品的售价
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 商品货品的售价
     * @param price 商品货品的售价
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 商品货品的规格列表
     * @return specifications 商品货品的规格列表
     */
    public String[] getSpecifications() {
        return specifications;
    }

    /**
     * 商品货品的规格列表
     * @param specifications 商品货品的规格列表
     */
    public void setSpecifications(String[] specifications) {
        this.specifications = specifications;
    }

    /**
     * 商品货品图片或者商品图片
     * @return pic_url 商品货品图片或者商品图片
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 商品货品图片或者商品图片
     * @param picUrl 商品货品图片或者商品图片
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * 订单商品评论，如果是-1，则超期不能评价；如果是0，则可以评价；如果是1，则追加评价；如果其他值，则是comment表里面的评论ID。
     * @return comment 订单商品评论，如果是-1，则超期不能评价；如果是0，则可以评价；如果是1，则追加评价；如果其他值，则是comment表里面的评论ID。
     */
    public Integer getComment() {
        return comment;
    }

    /**
     * 订单商品评论，如果是-1，则超期不能评价；如果是0，则可以评价；如果是1，则追加评价；如果其他值，则是comment表里面的评论ID。
     * @param comment 订单商品评论，如果是-1，则超期不能评价；如果是0，则可以评价；如果是1，则追加评价；如果其他值，则是comment表里面的评论ID。
     */
    public void setComment(Integer comment) {
        this.comment = comment;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 订单商品表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 订单商品表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", goodsName=").append(goodsName);
        sb.append(", goodsSn=").append(goodsSn);
        sb.append(", productId=").append(productId);
        sb.append(", number=").append(number);
        sb.append(", price=").append(price);
        sb.append(", specifications=").append(specifications);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", comment=").append(comment);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 订单商品表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallOrderGoods other = (LitemallOrderGoods) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getGoodsId() == null ? other.getGoodsId() == null : this.getGoodsId().equals(other.getGoodsId()))
            && (this.getGoodsName() == null ? other.getGoodsName() == null : this.getGoodsName().equals(other.getGoodsName()))
            && (this.getGoodsSn() == null ? other.getGoodsSn() == null : this.getGoodsSn().equals(other.getGoodsSn()))
            && (this.getProductId() == null ? other.getProductId() == null : this.getProductId().equals(other.getProductId()))
            && (this.getNumber() == null ? other.getNumber() == null : this.getNumber().equals(other.getNumber()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (Arrays.equals(this.getSpecifications(), other.getSpecifications()))
            && (this.getPicUrl() == null ? other.getPicUrl() == null : this.getPicUrl().equals(other.getPicUrl()))
            && (this.getComment() == null ? other.getComment() == null : this.getComment().equals(other.getComment()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()));
    }

    /**
     * 订单商品表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getGoodsId() == null) ? 0 : getGoodsId().hashCode());
        result = prime * result + ((getGoodsName() == null) ? 0 : getGoodsName().hashCode());
        result = prime * result + ((getGoodsSn() == null) ? 0 : getGoodsSn().hashCode());
        result = prime * result + ((getProductId() == null) ? 0 : getProductId().hashCode());
        result = prime * result + ((getNumber() == null) ? 0 : getNumber().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + (Arrays.hashCode(getSpecifications()));
        result = prime * result + ((getPicUrl() == null) ? 0 : getPicUrl().hashCode());
        result = prime * result + ((getComment() == null) ? 0 : getComment().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        return result;
    }

    /**
     * litemall_order_goods
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_order_goods
         */
        private final Boolean value;

        /**
         * litemall_order_goods
         */
        private final String name;

        /**
         * 订单商品表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 订单商品表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 订单商品表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 订单商品表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 订单商品表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 订单商品表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_order_goods
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        orderId("order_id", "orderId", "INTEGER", false),
        goodsId("goods_id", "goodsId", "INTEGER", false),
        goodsName("goods_name", "goodsName", "VARCHAR", false),
        goodsSn("goods_sn", "goodsSn", "VARCHAR", false),
        productId("product_id", "productId", "INTEGER", false),
        number("number", "number", "SMALLINT", true),
        price("price", "price", "DECIMAL", false),
        specifications("specifications", "specifications", "VARCHAR", false),
        picUrl("pic_url", "picUrl", "VARCHAR", false),
        comment("comment", "comment", "INTEGER", true),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false);

        /**
         * litemall_order_goods
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_order_goods
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_order_goods
         */
        private final String column;

        /**
         * litemall_order_goods
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_order_goods
         */
        private final String javaProperty;

        /**
         * litemall_order_goods
         */
        private final String jdbcType;

        /**
         * 订单商品表
         */
        public String value() {
            return this.column;
        }

        /**
         * 订单商品表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 订单商品表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 订单商品表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 订单商品表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 订单商品表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 订单商品表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 订单商品表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 订单商品表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 订单商品表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 订单商品表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}