package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_aftersale
 * Database Table Remarks : 
 *   售后表
 * @author linlinjava
 */
public class LitemallAftersale implements Serializable {
    /**
     * litemall_aftersale
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_aftersale
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    /**
     * 售后编号
     */
    private String aftersaleSn;

    /**
     * 订单ID
     */
    private Integer orderId;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款
     */
    private Short type;

    /**
     * 退款原因
     */
    private String reason;

    /**
     * 退款金额
     */
    private BigDecimal amount;

    /**
     * 退款凭证图片链接数组
     */
    private String[] pictures;

    /**
     * 退款说明
     */
    private String comment;

    /**
     * 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
     */
    private Short status;

    /**
     * 管理员操作时间
     */
    private LocalDateTime handleTime;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * litemall_aftersale
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 售后编号
     * @return aftersale_sn 售后编号
     */
    public String getAftersaleSn() {
        return aftersaleSn;
    }

    /**
     * 售后编号
     * @param aftersaleSn 售后编号
     */
    public void setAftersaleSn(String aftersaleSn) {
        this.aftersaleSn = aftersaleSn;
    }

    /**
     * 订单ID
     * @return order_id 订单ID
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 订单ID
     * @param orderId 订单ID
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 用户ID
     * @return user_id 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户ID
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款
     * @return type 售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款
     */
    public Short getType() {
        return type;
    }

    /**
     * 售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款
     * @param type 售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款
     */
    public void setType(Short type) {
        this.type = type;
    }

    /**
     * 退款原因
     * @return reason 退款原因
     */
    public String getReason() {
        return reason;
    }

    /**
     * 退款原因
     * @param reason 退款原因
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * 退款金额
     * @return amount 退款金额
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 退款金额
     * @param amount 退款金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 退款凭证图片链接数组
     * @return pictures 退款凭证图片链接数组
     */
    public String[] getPictures() {
        return pictures;
    }

    /**
     * 退款凭证图片链接数组
     * @param pictures 退款凭证图片链接数组
     */
    public void setPictures(String[] pictures) {
        this.pictures = pictures;
    }

    /**
     * 退款说明
     * @return comment 退款说明
     */
    public String getComment() {
        return comment;
    }

    /**
     * 退款说明
     * @param comment 退款说明
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
     * @return status 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
     */
    public Short getStatus() {
        return status;
    }

    /**
     * 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
     * @param status 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * 管理员操作时间
     * @return handle_time 管理员操作时间
     */
    public LocalDateTime getHandleTime() {
        return handleTime;
    }

    /**
     * 管理员操作时间
     * @param handleTime 管理员操作时间
     */
    public void setHandleTime(LocalDateTime handleTime) {
        this.handleTime = handleTime;
    }

    /**
     * 添加时间
     * @return add_time 添加时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 添加时间
     * @param addTime 添加时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 售后表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 售后表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", aftersaleSn=").append(aftersaleSn);
        sb.append(", orderId=").append(orderId);
        sb.append(", userId=").append(userId);
        sb.append(", type=").append(type);
        sb.append(", reason=").append(reason);
        sb.append(", amount=").append(amount);
        sb.append(", pictures=").append(pictures);
        sb.append(", comment=").append(comment);
        sb.append(", status=").append(status);
        sb.append(", handleTime=").append(handleTime);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 售后表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallAftersale other = (LitemallAftersale) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAftersaleSn() == null ? other.getAftersaleSn() == null : this.getAftersaleSn().equals(other.getAftersaleSn()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getReason() == null ? other.getReason() == null : this.getReason().equals(other.getReason()))
            && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
            && (Arrays.equals(this.getPictures(), other.getPictures()))
            && (this.getComment() == null ? other.getComment() == null : this.getComment().equals(other.getComment()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getHandleTime() == null ? other.getHandleTime() == null : this.getHandleTime().equals(other.getHandleTime()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()));
    }

    /**
     * 售后表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAftersaleSn() == null) ? 0 : getAftersaleSn().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getReason() == null) ? 0 : getReason().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + (Arrays.hashCode(getPictures()));
        result = prime * result + ((getComment() == null) ? 0 : getComment().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getHandleTime() == null) ? 0 : getHandleTime().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        return result;
    }

    /**
     * litemall_aftersale
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_aftersale
         */
        private final Boolean value;

        /**
         * litemall_aftersale
         */
        private final String name;

        /**
         * 售后表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 售后表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 售后表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 售后表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 售后表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 售后表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_aftersale
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        aftersaleSn("aftersale_sn", "aftersaleSn", "VARCHAR", false),
        orderId("order_id", "orderId", "INTEGER", false),
        userId("user_id", "userId", "INTEGER", false),
        type("type", "type", "SMALLINT", true),
        reason("reason", "reason", "VARCHAR", false),
        amount("amount", "amount", "DECIMAL", false),
        pictures("pictures", "pictures", "VARCHAR", false),
        comment("comment", "comment", "VARCHAR", true),
        status("status", "status", "SMALLINT", true),
        handleTime("handle_time", "handleTime", "TIMESTAMP", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false);

        /**
         * litemall_aftersale
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_aftersale
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_aftersale
         */
        private final String column;

        /**
         * litemall_aftersale
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_aftersale
         */
        private final String javaProperty;

        /**
         * litemall_aftersale
         */
        private final String jdbcType;

        /**
         * 售后表
         */
        public String value() {
            return this.column;
        }

        /**
         * 售后表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 售后表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 售后表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 售后表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 售后表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 售后表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 售后表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 售后表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 售后表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 售后表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}