package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallFootprint;
import org.linlinjava.litemall.db.example.LitemallFootprintExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallFootprintMapper {
    /**
     * 用户浏览足迹表
     */
    long countByExample(LitemallFootprintExample example);

    /**
     * 用户浏览足迹表
     */
    int deleteByExample(LitemallFootprintExample example);

    /**
     * 用户浏览足迹表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 用户浏览足迹表
     */
    int insert(LitemallFootprint record);

    /**
     * 用户浏览足迹表
     */
    int insertSelective(LitemallFootprint record);

    /**
     * 用户浏览足迹表
     */
    LitemallFootprint selectOneByExample(LitemallFootprintExample example);

    /**
     * 用户浏览足迹表
     */
    LitemallFootprint selectOneByExampleSelective(@Param("example") LitemallFootprintExample example, @Param("selective") LitemallFootprint.Column ... selective);

    /**
     * 用户浏览足迹表
     */
    List<LitemallFootprint> selectByExampleSelective(@Param("example") LitemallFootprintExample example, @Param("selective") LitemallFootprint.Column ... selective);

    /**
     * 用户浏览足迹表
     */
    List<LitemallFootprint> selectByExample(LitemallFootprintExample example);

    /**
     * 用户浏览足迹表
     */
    LitemallFootprint selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallFootprint.Column ... selective);

    /**
     * 用户浏览足迹表
     */
    LitemallFootprint selectByPrimaryKey(Integer id);

    /**
     * 用户浏览足迹表
     */
    LitemallFootprint selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 用户浏览足迹表
     */
    int updateByExampleSelective(@Param("record") LitemallFootprint record, @Param("example") LitemallFootprintExample example);

    /**
     * 用户浏览足迹表
     */
    int updateByExample(@Param("record") LitemallFootprint record, @Param("example") LitemallFootprintExample example);

    /**
     * 用户浏览足迹表
     */
    int updateByPrimaryKeySelective(LitemallFootprint record);

    /**
     * 用户浏览足迹表
     */
    int updateByPrimaryKey(LitemallFootprint record);

    /**
     * 用户浏览足迹表
     */
    int logicalDeleteByExample(@Param("example") LitemallFootprintExample example);

    /**
     * 用户浏览足迹表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}