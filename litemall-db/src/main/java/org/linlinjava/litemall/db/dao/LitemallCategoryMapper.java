package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallCategory;
import org.linlinjava.litemall.db.example.LitemallCategoryExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallCategoryMapper {
    /**
     * 类目表
     */
    long countByExample(LitemallCategoryExample example);

    /**
     * 类目表
     */
    int deleteByExample(LitemallCategoryExample example);

    /**
     * 类目表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 类目表
     */
    int insert(LitemallCategory record);

    /**
     * 类目表
     */
    int insertSelective(LitemallCategory record);

    /**
     * 类目表
     */
    LitemallCategory selectOneByExample(LitemallCategoryExample example);

    /**
     * 类目表
     */
    LitemallCategory selectOneByExampleSelective(@Param("example") LitemallCategoryExample example, @Param("selective") LitemallCategory.Column ... selective);

    /**
     * 类目表
     */
    List<LitemallCategory> selectByExampleSelective(@Param("example") LitemallCategoryExample example, @Param("selective") LitemallCategory.Column ... selective);

    /**
     * 类目表
     */
    List<LitemallCategory> selectByExample(LitemallCategoryExample example);

    /**
     * 类目表
     */
    LitemallCategory selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallCategory.Column ... selective);

    /**
     * 类目表
     */
    LitemallCategory selectByPrimaryKey(Integer id);

    /**
     * 类目表
     */
    LitemallCategory selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 类目表
     */
    int updateByExampleSelective(@Param("record") LitemallCategory record, @Param("example") LitemallCategoryExample example);

    /**
     * 类目表
     */
    int updateByExample(@Param("record") LitemallCategory record, @Param("example") LitemallCategoryExample example);

    /**
     * 类目表
     */
    int updateByPrimaryKeySelective(LitemallCategory record);

    /**
     * 类目表
     */
    int updateByPrimaryKey(LitemallCategory record);

    /**
     * 类目表
     */
    int logicalDeleteByExample(@Param("example") LitemallCategoryExample example);

    /**
     * 类目表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}