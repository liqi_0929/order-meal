package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallCart;
import org.linlinjava.litemall.db.example.LitemallCartExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallCartMapper {
    /**
     * 购物车商品表
     */
    long countByExample(LitemallCartExample example);

    /**
     * 购物车商品表
     */
    int deleteByExample(LitemallCartExample example);

    /**
     * 购物车商品表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 购物车商品表
     */
    int insert(LitemallCart record);

    /**
     * 购物车商品表
     */
    int insertSelective(LitemallCart record);

    /**
     * 购物车商品表
     */
    LitemallCart selectOneByExample(LitemallCartExample example);

    /**
     * 购物车商品表
     */
    LitemallCart selectOneByExampleSelective(@Param("example") LitemallCartExample example, @Param("selective") LitemallCart.Column ... selective);

    /**
     * 购物车商品表
     */
    List<LitemallCart> selectByExampleSelective(@Param("example") LitemallCartExample example, @Param("selective") LitemallCart.Column ... selective);

    /**
     * 购物车商品表
     */
    List<LitemallCart> selectByExample(LitemallCartExample example);

    /**
     * 购物车商品表
     */
    LitemallCart selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallCart.Column ... selective);

    /**
     * 购物车商品表
     */
    LitemallCart selectByPrimaryKey(Integer id);

    /**
     * 购物车商品表
     */
    LitemallCart selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 购物车商品表
     */
    int updateByExampleSelective(@Param("record") LitemallCart record, @Param("example") LitemallCartExample example);

    /**
     * 购物车商品表
     */
    int updateByExample(@Param("record") LitemallCart record, @Param("example") LitemallCartExample example);

    /**
     * 购物车商品表
     */
    int updateByPrimaryKeySelective(LitemallCart record);

    /**
     * 购物车商品表
     */
    int updateByPrimaryKey(LitemallCart record);

    /**
     * 购物车商品表
     */
    int logicalDeleteByExample(@Param("example") LitemallCartExample example);

    /**
     * 购物车商品表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}