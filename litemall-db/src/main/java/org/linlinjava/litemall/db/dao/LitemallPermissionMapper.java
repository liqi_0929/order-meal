package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallPermission;
import org.linlinjava.litemall.db.example.LitemallPermissionExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallPermissionMapper {
    /**
     * 权限表
     */
    long countByExample(LitemallPermissionExample example);

    /**
     * 权限表
     */
    int deleteByExample(LitemallPermissionExample example);

    /**
     * 权限表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 权限表
     */
    int insert(LitemallPermission record);

    /**
     * 权限表
     */
    int insertSelective(LitemallPermission record);

    /**
     * 权限表
     */
    LitemallPermission selectOneByExample(LitemallPermissionExample example);

    /**
     * 权限表
     */
    LitemallPermission selectOneByExampleSelective(@Param("example") LitemallPermissionExample example, @Param("selective") LitemallPermission.Column ... selective);

    /**
     * 权限表
     */
    List<LitemallPermission> selectByExampleSelective(@Param("example") LitemallPermissionExample example, @Param("selective") LitemallPermission.Column ... selective);

    /**
     * 权限表
     */
    List<LitemallPermission> selectByExample(LitemallPermissionExample example);

    /**
     * 权限表
     */
    LitemallPermission selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallPermission.Column ... selective);

    /**
     * 权限表
     */
    LitemallPermission selectByPrimaryKey(Integer id);

    /**
     * 权限表
     */
    LitemallPermission selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 权限表
     */
    int updateByExampleSelective(@Param("record") LitemallPermission record, @Param("example") LitemallPermissionExample example);

    /**
     * 权限表
     */
    int updateByExample(@Param("record") LitemallPermission record, @Param("example") LitemallPermissionExample example);

    /**
     * 权限表
     */
    int updateByPrimaryKeySelective(LitemallPermission record);

    /**
     * 权限表
     */
    int updateByPrimaryKey(LitemallPermission record);

    /**
     * 权限表
     */
    int logicalDeleteByExample(@Param("example") LitemallPermissionExample example);

    /**
     * 权限表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}