package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallNotice;
import org.linlinjava.litemall.db.example.LitemallNoticeExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallNoticeMapper {
    /**
     * 通知表
     */
    long countByExample(LitemallNoticeExample example);

    /**
     * 通知表
     */
    int deleteByExample(LitemallNoticeExample example);

    /**
     * 通知表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 通知表
     */
    int insert(LitemallNotice record);

    /**
     * 通知表
     */
    int insertSelective(LitemallNotice record);

    /**
     * 通知表
     */
    LitemallNotice selectOneByExample(LitemallNoticeExample example);

    /**
     * 通知表
     */
    LitemallNotice selectOneByExampleSelective(@Param("example") LitemallNoticeExample example, @Param("selective") LitemallNotice.Column ... selective);

    /**
     * 通知表
     */
    List<LitemallNotice> selectByExampleSelective(@Param("example") LitemallNoticeExample example, @Param("selective") LitemallNotice.Column ... selective);

    /**
     * 通知表
     */
    List<LitemallNotice> selectByExample(LitemallNoticeExample example);

    /**
     * 通知表
     */
    LitemallNotice selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallNotice.Column ... selective);

    /**
     * 通知表
     */
    LitemallNotice selectByPrimaryKey(Integer id);

    /**
     * 通知表
     */
    LitemallNotice selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 通知表
     */
    int updateByExampleSelective(@Param("record") LitemallNotice record, @Param("example") LitemallNoticeExample example);

    /**
     * 通知表
     */
    int updateByExample(@Param("record") LitemallNotice record, @Param("example") LitemallNoticeExample example);

    /**
     * 通知表
     */
    int updateByPrimaryKeySelective(LitemallNotice record);

    /**
     * 通知表
     */
    int updateByPrimaryKey(LitemallNotice record);

    /**
     * 通知表
     */
    int logicalDeleteByExample(@Param("example") LitemallNoticeExample example);

    /**
     * 通知表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}