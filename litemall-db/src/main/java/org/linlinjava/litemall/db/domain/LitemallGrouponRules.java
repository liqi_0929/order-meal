package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_groupon_rules
 * Database Table Remarks : 
 *   团购规则表
 * @author linlinjava
 */
public class LitemallGrouponRules implements Serializable {
    /**
     * litemall_groupon_rules
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_groupon_rules
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    /**
     * 商品表的商品ID
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品图片或者商品货品图片
     */
    private String picUrl;

    /**
     * 优惠金额
     */
    private BigDecimal discount;

    /**
     * 达到优惠条件的人数
     */
    private Integer discountMember;

    /**
     * 团购过期时间
     */
    private LocalDateTime expireTime;

    /**
     * 团购规则状态，正常上线则0，到期自动下线则1，管理手动下线则2
     */
    private Short status;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * litemall_groupon_rules
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 商品表的商品ID
     * @return goods_id 商品表的商品ID
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 商品表的商品ID
     * @param goodsId 商品表的商品ID
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 商品名称
     * @return goods_name 商品名称
     */
    public String getGoodsName() {
        return goodsName;
    }

    /**
     * 商品名称
     * @param goodsName 商品名称
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * 商品图片或者商品货品图片
     * @return pic_url 商品图片或者商品货品图片
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 商品图片或者商品货品图片
     * @param picUrl 商品图片或者商品货品图片
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * 优惠金额
     * @return discount 优惠金额
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * 优惠金额
     * @param discount 优惠金额
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * 达到优惠条件的人数
     * @return discount_member 达到优惠条件的人数
     */
    public Integer getDiscountMember() {
        return discountMember;
    }

    /**
     * 达到优惠条件的人数
     * @param discountMember 达到优惠条件的人数
     */
    public void setDiscountMember(Integer discountMember) {
        this.discountMember = discountMember;
    }

    /**
     * 团购过期时间
     * @return expire_time 团购过期时间
     */
    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    /**
     * 团购过期时间
     * @param expireTime 团购过期时间
     */
    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }

    /**
     * 团购规则状态，正常上线则0，到期自动下线则1，管理手动下线则2
     * @return status 团购规则状态，正常上线则0，到期自动下线则1，管理手动下线则2
     */
    public Short getStatus() {
        return status;
    }

    /**
     * 团购规则状态，正常上线则0，到期自动下线则1，管理手动下线则2
     * @param status 团购规则状态，正常上线则0，到期自动下线则1，管理手动下线则2
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 团购规则表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 团购规则表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", goodsName=").append(goodsName);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", discount=").append(discount);
        sb.append(", discountMember=").append(discountMember);
        sb.append(", expireTime=").append(expireTime);
        sb.append(", status=").append(status);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 团购规则表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallGrouponRules other = (LitemallGrouponRules) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getGoodsId() == null ? other.getGoodsId() == null : this.getGoodsId().equals(other.getGoodsId()))
            && (this.getGoodsName() == null ? other.getGoodsName() == null : this.getGoodsName().equals(other.getGoodsName()))
            && (this.getPicUrl() == null ? other.getPicUrl() == null : this.getPicUrl().equals(other.getPicUrl()))
            && (this.getDiscount() == null ? other.getDiscount() == null : this.getDiscount().equals(other.getDiscount()))
            && (this.getDiscountMember() == null ? other.getDiscountMember() == null : this.getDiscountMember().equals(other.getDiscountMember()))
            && (this.getExpireTime() == null ? other.getExpireTime() == null : this.getExpireTime().equals(other.getExpireTime()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()));
    }

    /**
     * 团购规则表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getGoodsId() == null) ? 0 : getGoodsId().hashCode());
        result = prime * result + ((getGoodsName() == null) ? 0 : getGoodsName().hashCode());
        result = prime * result + ((getPicUrl() == null) ? 0 : getPicUrl().hashCode());
        result = prime * result + ((getDiscount() == null) ? 0 : getDiscount().hashCode());
        result = prime * result + ((getDiscountMember() == null) ? 0 : getDiscountMember().hashCode());
        result = prime * result + ((getExpireTime() == null) ? 0 : getExpireTime().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        return result;
    }

    /**
     * litemall_groupon_rules
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_groupon_rules
         */
        private final Boolean value;

        /**
         * litemall_groupon_rules
         */
        private final String name;

        /**
         * 团购规则表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 团购规则表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 团购规则表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 团购规则表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 团购规则表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 团购规则表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_groupon_rules
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        goodsId("goods_id", "goodsId", "INTEGER", false),
        goodsName("goods_name", "goodsName", "VARCHAR", false),
        picUrl("pic_url", "picUrl", "VARCHAR", false),
        discount("discount", "discount", "DECIMAL", false),
        discountMember("discount_member", "discountMember", "INTEGER", false),
        expireTime("expire_time", "expireTime", "TIMESTAMP", false),
        status("status", "status", "SMALLINT", true),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false);

        /**
         * litemall_groupon_rules
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_groupon_rules
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_groupon_rules
         */
        private final String column;

        /**
         * litemall_groupon_rules
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_groupon_rules
         */
        private final String javaProperty;

        /**
         * litemall_groupon_rules
         */
        private final String jdbcType;

        /**
         * 团购规则表
         */
        public String value() {
            return this.column;
        }

        /**
         * 团购规则表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 团购规则表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 团购规则表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 团购规则表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 团购规则表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 团购规则表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 团购规则表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 团购规则表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 团购规则表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 团购规则表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}