package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallCollect;
import org.linlinjava.litemall.db.example.LitemallCollectExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallCollectMapper {
    /**
     * 收藏表
     */
    long countByExample(LitemallCollectExample example);

    /**
     * 收藏表
     */
    int deleteByExample(LitemallCollectExample example);

    /**
     * 收藏表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 收藏表
     */
    int insert(LitemallCollect record);

    /**
     * 收藏表
     */
    int insertSelective(LitemallCollect record);

    /**
     * 收藏表
     */
    LitemallCollect selectOneByExample(LitemallCollectExample example);

    /**
     * 收藏表
     */
    LitemallCollect selectOneByExampleSelective(@Param("example") LitemallCollectExample example, @Param("selective") LitemallCollect.Column ... selective);

    /**
     * 收藏表
     */
    List<LitemallCollect> selectByExampleSelective(@Param("example") LitemallCollectExample example, @Param("selective") LitemallCollect.Column ... selective);

    /**
     * 收藏表
     */
    List<LitemallCollect> selectByExample(LitemallCollectExample example);

    /**
     * 收藏表
     */
    LitemallCollect selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallCollect.Column ... selective);

    /**
     * 收藏表
     */
    LitemallCollect selectByPrimaryKey(Integer id);

    /**
     * 收藏表
     */
    LitemallCollect selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 收藏表
     */
    int updateByExampleSelective(@Param("record") LitemallCollect record, @Param("example") LitemallCollectExample example);

    /**
     * 收藏表
     */
    int updateByExample(@Param("record") LitemallCollect record, @Param("example") LitemallCollectExample example);

    /**
     * 收藏表
     */
    int updateByPrimaryKeySelective(LitemallCollect record);

    /**
     * 收藏表
     */
    int updateByPrimaryKey(LitemallCollect record);

    /**
     * 收藏表
     */
    int logicalDeleteByExample(@Param("example") LitemallCollectExample example);

    /**
     * 收藏表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}