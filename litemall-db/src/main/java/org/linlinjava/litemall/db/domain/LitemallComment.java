package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_comment
 * Database Table Remarks : 
 *   评论表
 * @author linlinjava
 */
public class LitemallComment implements Serializable {
    /**
     * litemall_comment
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_comment
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    /**
     * 如果type=0，则是商品评论；如果是type=1，则是专题评论。
     */
    private Integer valueId;

    /**
     * 评论类型，如果type=0，则是商品评论；如果是type=1，则是专题评论；
     */
    private Byte type;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 管理员回复内容
     */
    private String adminContent;

    /**
     * 用户表的用户ID
     */
    private Integer userId;

    /**
     * 是否含有图片
     */
    private Boolean hasPicture;

    /**
     * 图片地址列表，采用JSON数组格式
     */
    private String[] picUrls;

    /**
     * 评分， 1-5
     */
    private Short star;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * litemall_comment
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 如果type=0，则是商品评论；如果是type=1，则是专题评论。
     * @return value_id 如果type=0，则是商品评论；如果是type=1，则是专题评论。
     */
    public Integer getValueId() {
        return valueId;
    }

    /**
     * 如果type=0，则是商品评论；如果是type=1，则是专题评论。
     * @param valueId 如果type=0，则是商品评论；如果是type=1，则是专题评论。
     */
    public void setValueId(Integer valueId) {
        this.valueId = valueId;
    }

    /**
     * 评论类型，如果type=0，则是商品评论；如果是type=1，则是专题评论；
     * @return type 评论类型，如果type=0，则是商品评论；如果是type=1，则是专题评论；
     */
    public Byte getType() {
        return type;
    }

    /**
     * 评论类型，如果type=0，则是商品评论；如果是type=1，则是专题评论；
     * @param type 评论类型，如果type=0，则是商品评论；如果是type=1，则是专题评论；
     */
    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 评论内容
     * @return content 评论内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 评论内容
     * @param content 评论内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 管理员回复内容
     * @return admin_content 管理员回复内容
     */
    public String getAdminContent() {
        return adminContent;
    }

    /**
     * 管理员回复内容
     * @param adminContent 管理员回复内容
     */
    public void setAdminContent(String adminContent) {
        this.adminContent = adminContent;
    }

    /**
     * 用户表的用户ID
     * @return user_id 用户表的用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户表的用户ID
     * @param userId 用户表的用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 是否含有图片
     * @return has_picture 是否含有图片
     */
    public Boolean getHasPicture() {
        return hasPicture;
    }

    /**
     * 是否含有图片
     * @param hasPicture 是否含有图片
     */
    public void setHasPicture(Boolean hasPicture) {
        this.hasPicture = hasPicture;
    }

    /**
     * 图片地址列表，采用JSON数组格式
     * @return pic_urls 图片地址列表，采用JSON数组格式
     */
    public String[] getPicUrls() {
        return picUrls;
    }

    /**
     * 图片地址列表，采用JSON数组格式
     * @param picUrls 图片地址列表，采用JSON数组格式
     */
    public void setPicUrls(String[] picUrls) {
        this.picUrls = picUrls;
    }

    /**
     * 评分， 1-5
     * @return star 评分， 1-5
     */
    public Short getStar() {
        return star;
    }

    /**
     * 评分， 1-5
     * @param star 评分， 1-5
     */
    public void setStar(Short star) {
        this.star = star;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 评论表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 评论表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", valueId=").append(valueId);
        sb.append(", type=").append(type);
        sb.append(", content=").append(content);
        sb.append(", adminContent=").append(adminContent);
        sb.append(", userId=").append(userId);
        sb.append(", hasPicture=").append(hasPicture);
        sb.append(", picUrls=").append(picUrls);
        sb.append(", star=").append(star);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 评论表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallComment other = (LitemallComment) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getValueId() == null ? other.getValueId() == null : this.getValueId().equals(other.getValueId()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (this.getAdminContent() == null ? other.getAdminContent() == null : this.getAdminContent().equals(other.getAdminContent()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getHasPicture() == null ? other.getHasPicture() == null : this.getHasPicture().equals(other.getHasPicture()))
            && (Arrays.equals(this.getPicUrls(), other.getPicUrls()))
            && (this.getStar() == null ? other.getStar() == null : this.getStar().equals(other.getStar()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()));
    }

    /**
     * 评论表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getValueId() == null) ? 0 : getValueId().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getAdminContent() == null) ? 0 : getAdminContent().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getHasPicture() == null) ? 0 : getHasPicture().hashCode());
        result = prime * result + (Arrays.hashCode(getPicUrls()));
        result = prime * result + ((getStar() == null) ? 0 : getStar().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        return result;
    }

    /**
     * litemall_comment
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_comment
         */
        private final Boolean value;

        /**
         * litemall_comment
         */
        private final String name;

        /**
         * 评论表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 评论表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 评论表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 评论表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 评论表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 评论表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_comment
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        valueId("value_id", "valueId", "INTEGER", false),
        type("type", "type", "TINYINT", true),
        content("content", "content", "VARCHAR", false),
        adminContent("admin_content", "adminContent", "VARCHAR", false),
        userId("user_id", "userId", "INTEGER", false),
        hasPicture("has_picture", "hasPicture", "BIT", false),
        picUrls("pic_urls", "picUrls", "VARCHAR", false),
        star("star", "star", "SMALLINT", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false);

        /**
         * litemall_comment
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_comment
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_comment
         */
        private final String column;

        /**
         * litemall_comment
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_comment
         */
        private final String javaProperty;

        /**
         * litemall_comment
         */
        private final String jdbcType;

        /**
         * 评论表
         */
        public String value() {
            return this.column;
        }

        /**
         * 评论表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 评论表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 评论表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 评论表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 评论表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 评论表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 评论表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 评论表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 评论表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 评论表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}