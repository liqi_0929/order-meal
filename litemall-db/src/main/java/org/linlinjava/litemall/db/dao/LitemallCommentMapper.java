package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallComment;
import org.linlinjava.litemall.db.example.LitemallCommentExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallCommentMapper {
    /**
     * 评论表
     */
    long countByExample(LitemallCommentExample example);

    /**
     * 评论表
     */
    int deleteByExample(LitemallCommentExample example);

    /**
     * 评论表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 评论表
     */
    int insert(LitemallComment record);

    /**
     * 评论表
     */
    int insertSelective(LitemallComment record);

    /**
     * 评论表
     */
    LitemallComment selectOneByExample(LitemallCommentExample example);

    /**
     * 评论表
     */
    LitemallComment selectOneByExampleSelective(@Param("example") LitemallCommentExample example, @Param("selective") LitemallComment.Column ... selective);

    /**
     * 评论表
     */
    List<LitemallComment> selectByExampleSelective(@Param("example") LitemallCommentExample example, @Param("selective") LitemallComment.Column ... selective);

    /**
     * 评论表
     */
    List<LitemallComment> selectByExample(LitemallCommentExample example);

    /**
     * 评论表
     */
    LitemallComment selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallComment.Column ... selective);

    /**
     * 评论表
     */
    LitemallComment selectByPrimaryKey(Integer id);

    /**
     * 评论表
     */
    LitemallComment selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 评论表
     */
    int updateByExampleSelective(@Param("record") LitemallComment record, @Param("example") LitemallCommentExample example);

    /**
     * 评论表
     */
    int updateByExample(@Param("record") LitemallComment record, @Param("example") LitemallCommentExample example);

    /**
     * 评论表
     */
    int updateByPrimaryKeySelective(LitemallComment record);

    /**
     * 评论表
     */
    int updateByPrimaryKey(LitemallComment record);

    /**
     * 评论表
     */
    int logicalDeleteByExample(@Param("example") LitemallCommentExample example);

    /**
     * 评论表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}