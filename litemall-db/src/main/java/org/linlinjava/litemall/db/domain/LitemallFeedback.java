package org.linlinjava.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_feedback
 * Database Table Remarks : 
 *   意见反馈表
 * @author linlinjava
 */
public class LitemallFeedback implements Serializable {
    /**
     * litemall_feedback
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_feedback
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 
     */
    private Integer id;

    /**
     * 用户表的用户ID
     */
    private Integer userId;

    /**
     * 用户名称
     */
    private String username;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 反馈类型
     */
    private String feedType;

    /**
     * 反馈内容
     */
    private String content;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 是否含有图片
     */
    private Boolean hasPicture;

    /**
     * 图片地址列表，采用JSON数组格式
     */
    private String[] picUrls;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * litemall_feedback
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 用户表的用户ID
     * @return user_id 用户表的用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户表的用户ID
     * @param userId 用户表的用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 用户名称
     * @return username 用户名称
     */
    public String getUsername() {
        return username;
    }

    /**
     * 用户名称
     * @param username 用户名称
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 手机号
     * @return mobile 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 手机号
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 反馈类型
     * @return feed_type 反馈类型
     */
    public String getFeedType() {
        return feedType;
    }

    /**
     * 反馈类型
     * @param feedType 反馈类型
     */
    public void setFeedType(String feedType) {
        this.feedType = feedType;
    }

    /**
     * 反馈内容
     * @return content 反馈内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 反馈内容
     * @param content 反馈内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 状态
     * @return status 状态
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 状态
     * @param status 状态
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 是否含有图片
     * @return has_picture 是否含有图片
     */
    public Boolean getHasPicture() {
        return hasPicture;
    }

    /**
     * 是否含有图片
     * @param hasPicture 是否含有图片
     */
    public void setHasPicture(Boolean hasPicture) {
        this.hasPicture = hasPicture;
    }

    /**
     * 图片地址列表，采用JSON数组格式
     * @return pic_urls 图片地址列表，采用JSON数组格式
     */
    public String[] getPicUrls() {
        return picUrls;
    }

    /**
     * 图片地址列表，采用JSON数组格式
     * @param picUrls 图片地址列表，采用JSON数组格式
     */
    public void setPicUrls(String[] picUrls) {
        this.picUrls = picUrls;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 意见反馈表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 意见反馈表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", username=").append(username);
        sb.append(", mobile=").append(mobile);
        sb.append(", feedType=").append(feedType);
        sb.append(", content=").append(content);
        sb.append(", status=").append(status);
        sb.append(", hasPicture=").append(hasPicture);
        sb.append(", picUrls=").append(picUrls);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 意见反馈表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallFeedback other = (LitemallFeedback) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getUsername() == null ? other.getUsername() == null : this.getUsername().equals(other.getUsername()))
            && (this.getMobile() == null ? other.getMobile() == null : this.getMobile().equals(other.getMobile()))
            && (this.getFeedType() == null ? other.getFeedType() == null : this.getFeedType().equals(other.getFeedType()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getHasPicture() == null ? other.getHasPicture() == null : this.getHasPicture().equals(other.getHasPicture()))
            && (Arrays.equals(this.getPicUrls(), other.getPicUrls()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()));
    }

    /**
     * 意见反馈表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
        result = prime * result + ((getMobile() == null) ? 0 : getMobile().hashCode());
        result = prime * result + ((getFeedType() == null) ? 0 : getFeedType().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getHasPicture() == null) ? 0 : getHasPicture().hashCode());
        result = prime * result + (Arrays.hashCode(getPicUrls()));
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        return result;
    }

    /**
     * litemall_feedback
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_feedback
         */
        private final Boolean value;

        /**
         * litemall_feedback
         */
        private final String name;

        /**
         * 意见反馈表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 意见反馈表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 意见反馈表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 意见反馈表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 意见反馈表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 意见反馈表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_feedback
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        userId("user_id", "userId", "INTEGER", false),
        username("username", "username", "VARCHAR", false),
        mobile("mobile", "mobile", "VARCHAR", false),
        feedType("feed_type", "feedType", "VARCHAR", false),
        content("content", "content", "VARCHAR", false),
        status("status", "status", "INTEGER", true),
        hasPicture("has_picture", "hasPicture", "BIT", false),
        picUrls("pic_urls", "picUrls", "VARCHAR", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false);

        /**
         * litemall_feedback
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_feedback
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_feedback
         */
        private final String column;

        /**
         * litemall_feedback
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_feedback
         */
        private final String javaProperty;

        /**
         * litemall_feedback
         */
        private final String jdbcType;

        /**
         * 意见反馈表
         */
        public String value() {
            return this.column;
        }

        /**
         * 意见反馈表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 意见反馈表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 意见反馈表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 意见反馈表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 意见反馈表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 意见反馈表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 意见反馈表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 意见反馈表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 意见反馈表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 意见反馈表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}