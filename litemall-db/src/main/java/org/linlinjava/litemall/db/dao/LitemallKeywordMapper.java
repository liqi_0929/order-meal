package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallKeyword;
import org.linlinjava.litemall.db.example.LitemallKeywordExample;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LitemallKeywordMapper {
    /**
     * 关键字表
     */
    long countByExample(LitemallKeywordExample example);

    /**
     * 关键字表
     */
    int deleteByExample(LitemallKeywordExample example);

    /**
     * 关键字表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 关键字表
     */
    int insert(LitemallKeyword record);

    /**
     * 关键字表
     */
    int insertSelective(LitemallKeyword record);

    /**
     * 关键字表
     */
    LitemallKeyword selectOneByExample(LitemallKeywordExample example);

    /**
     * 关键字表
     */
    LitemallKeyword selectOneByExampleSelective(@Param("example") LitemallKeywordExample example, @Param("selective") LitemallKeyword.Column ... selective);

    /**
     * 关键字表
     */
    List<LitemallKeyword> selectByExampleSelective(@Param("example") LitemallKeywordExample example, @Param("selective") LitemallKeyword.Column ... selective);

    /**
     * 关键字表
     */
    List<LitemallKeyword> selectByExample(LitemallKeywordExample example);

    /**
     * 关键字表
     */
    LitemallKeyword selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallKeyword.Column ... selective);

    /**
     * 关键字表
     */
    LitemallKeyword selectByPrimaryKey(Integer id);

    /**
     * 关键字表
     */
    LitemallKeyword selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 关键字表
     */
    int updateByExampleSelective(@Param("record") LitemallKeyword record, @Param("example") LitemallKeywordExample example);

    /**
     * 关键字表
     */
    int updateByExample(@Param("record") LitemallKeyword record, @Param("example") LitemallKeywordExample example);

    /**
     * 关键字表
     */
    int updateByPrimaryKeySelective(LitemallKeyword record);

    /**
     * 关键字表
     */
    int updateByPrimaryKey(LitemallKeyword record);

    /**
     * 关键字表
     */
    int logicalDeleteByExample(@Param("example") LitemallKeywordExample example);

    /**
     * 关键字表
     */
    int logicalDeleteByPrimaryKey(Integer id);
}