package org.linlinjava.litemall.admin.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.core.util.JacksonUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.core.validator.Order;
import org.linlinjava.litemall.core.validator.Sort;
import org.linlinjava.litemall.db.domain.LitemallQrcode;
import org.linlinjava.litemall.db.service.LitemallQrcodeService;
import org.linlinjava.litemall.db.util.QRCodeGenerator;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static org.linlinjava.litemall.admin.util.AdminResponseCode.ADMIN_NAME_EXIST;

/**
 * 二维码配置
 */
@RestController
@RequestMapping("/admin/qrcode")
@Validated
public class AdminQrcodeController {

    @Resource
    private LitemallQrcodeService qrcodeService;

    @RequiresPermissions("admin:qrcode:list")
    @RequiresPermissionsDesc(menu = {"店铺管理", "桌面二维码"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
    	List<LitemallQrcode> qrcodeList = qrcodeService.queryList(page, limit, sort, order);
        return ResponseUtil.okList(qrcodeList);
    }
    

    @RequiresPermissions("admin:qrcode:create")
    @RequiresPermissionsDesc(menu = {"店铺管理", "桌面二维码"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody String body) {
        String tableNo = JacksonUtil.parseString(body, "tableNo");
        String httpTcp = JacksonUtil.parseString(body, "httpTcp");
        Integer imgSize = JacksonUtil.parseInteger(body, "imgSize");

        if (StringUtils.isEmpty(tableNo) || StringUtils.isEmpty(httpTcp) || imgSize == null) {
            return ResponseUtil.badArgument();
        }

        List<LitemallQrcode> qrcodeList = qrcodeService.findQrcode(tableNo);
        if (qrcodeList.size() > 0){
            return ResponseUtil.fail(ADMIN_NAME_EXIST, "桌号已经存在");
        }

        if (imgSize < 50){
            imgSize = 50;
        }else if(imgSize > 1000){
            imgSize = 1000;
        }
        byte[] picBlob = QRCodeGenerator.createPicBlob(httpTcp, tableNo, imgSize);
        if (picBlob == null){
            return ResponseUtil.fail(ADMIN_NAME_EXIST, "二维码生成失败");
        }

        LitemallQrcode qrcode = new LitemallQrcode();
		qrcode.setTableNo(tableNo);
		qrcode.setPicBlob(picBlob);
		qrcode.setHttpTcp(httpTcp);
        qrcode.setImgSize(imgSize);
        qrcodeService.add(qrcode);
        return ResponseUtil.ok(qrcode);
    }

    @RequiresPermissions("admin:qrcode:delete")
    @RequiresPermissionsDesc(menu = {"店铺管理", "桌面二维码"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallQrcode qrcode) {
        if (qrcode == null){
            return ResponseUtil.badArgument();
        }
        qrcodeService.deleteById(qrcode.getId());
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:qrcode:update")
    @RequiresPermissionsDesc(menu = {"店铺管理", "桌面二维码"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody LitemallQrcode qrcode) {
        if (qrcode == null){
            return ResponseUtil.badArgument();
        }

        Integer imgSize = qrcode.getImgSize();
        String tableNo = qrcode.getTableNo();
        String httpTcp = qrcode.getHttpTcp();
        if (StringUtils.isEmpty(tableNo) || StringUtils.isEmpty(httpTcp) || imgSize == null) {
            return ResponseUtil.badArgument();
        }

        if (imgSize < 50){
            imgSize = 50;
        }else if(imgSize > 1000){
            imgSize = 1000;
        }
        byte[] picBlob = QRCodeGenerator.createPicBlob(httpTcp, tableNo, imgSize);
        if (picBlob == null){
            return ResponseUtil.fail(ADMIN_NAME_EXIST, "二维码修改失败");
        }
        qrcode.setImgSize(imgSize);
        qrcode.setPicBlob(picBlob);
        qrcodeService.updateBlobById(qrcode);
        return ResponseUtil.ok(qrcode);
    }
}
