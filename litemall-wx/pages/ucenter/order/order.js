var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

Page({
  data: {
    tableName:['全部','待付款','待发货','待收货','待评价'],
    scrollLeft:0,
    orderList: [],
    showType: 0,
    page: 1,
    limit: 10,
    totalPages: 1,
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    let that = this
    if (options.tab) {
      this.setData({
        showType: parseInt(options.tab)
      });
      this.getOrderList();
    }else{
      try {
        var tab = wx.getStorageSync('tab');
        that.setData({
          showType: tab
        });
      } catch (e) {}
      this.getOrderList();
    }

    
  },
  getOrderList() {
    let that = this;
    util.request(api.OrderList, {
      showType: that.data.showType,
      page: that.data.page,
      limit: that.data.limit
    }).then(function(res) {
      if (res.errno === 0) {
        that.setData({
          orderList: that.data.orderList.concat(res.data.list),
          totalPages: res.data.pages
        });
      }
    });
  },
  onReachBottom() {
    if (this.data.totalPages > this.data.page) {
      this.setData({
        page: this.data.page + 1
      });
      this.getOrderList();
    } else {
      wx.showToast({
        title: '没有更多订单了',
        icon: 'none',
        duration: 2000
      });
      return false;
    }
  },



  switchTab: function(e) {
    let showType = e.currentTarget.dataset.index;
    this.setData({
      scrollLeft: (e.currentTarget.dataset.id-1)*60,
      orderList: [],
      showType: showType,
      page: 1,
      limit: 10,
      totalPages: 1
    });
    this.getOrderList();
  },
  
  onReady: function() {
    // 页面渲染完成
  },
  onShow: function() {
    // 页面显示
  },
  onHide: function() {
    // 页面隐藏
  },
  onUnload: function() {
    // 页面关闭 
    wx.navigateBack({
      delta: 1
    })
  }
})