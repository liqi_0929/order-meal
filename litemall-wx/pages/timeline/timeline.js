const util = require('../../utils/util.js');
const api = require('../../config/api.js');
const user = require('../../utils/user.js');
//获取应用实例
const app = getApp();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        hasLogin: false,
        Administrator: false,
        userInfo: {},
        ColorList: app.globalData.ColorList,
        TabCur: 0,
        scrollLeft:0,
        tableName:['动态','订单'],
        modalName:'',
        timelineVoList:[],//发布列表
        orderVoList:[],//订单列表
        timeVoId: 0,
        index:0,//外层循环下标
        vindex:0,//内层循环下标
        limit: 10,
        orderVoPage: 1,
        orderVoTotalPages: 1,
        timelineVoPage: 1,
        timelineVoTotalPages: 1
    },

    //打开弹窗
    showModal(e) {
      console.log(e)
      this.setData({
        timeVo:e.currentTarget.dataset.timevo,
        index:e.currentTarget.dataset.index,
        vindex:e.currentTarget.dataset.vindex,
        modalName: 'menuModal',
      })
    },
    //关闭弹窗
    hideModal(e) {
      this.setData({
        modalName: null
      })
    },


    //编辑
    editTimeLine: function (e) {
      let that = this;
      util.showErrorToast('无权限');
      that.hideModal(e);
    },

    //删除
    deleteTimeLine: function (e) {
      let that = this;
      let _timeVo = that.data.timeVo;
      util.request(api.TimeLineDelete, {
        timeVoId: _timeVo.id,
        userId: _timeVo.userId,
        isAdmin: that.data.Administrator,
      },'POST').then(function(res) {
        if (res.errno === 0) {
          wx.showToast({
            title: '删除成功',
            icon: 'success',
            duration: 2000//持续的时间
          })
          let index = that.data.index;
          let vindex = that.data.vindex;
          that.data.timelineVoList[index].timeLineVo.splice(vindex,1);
          //渲染数据
          that.setData({
            timelineVoList:that.data.timelineVoList
          }); 
        }else{
          util.showErrorToast(res.errmsg);
        }
      });
      that.hideModal(e);
    },

    //分页
    tabSelect(e) {
      this.setData({
        TabCur: e.currentTarget.dataset.id,
        scrollLeft: (e.currentTarget.dataset.id-1)*60
      })
    },


    //获取订单信息
    getOrderVoList(){
      let that = this;
      util.request(api.TimelinOrderList, {
        isAdmin:that.data.Administrator,
        page: that.data.orderVoPage,
        limit: that.data.limit
      }).then(function(res) {
        if (res.errno === 0) {
          that.setData({
            orderVoList: that.data.orderVoList.concat(res.data.list),
            orderVoTotalPages: res.data.pages
          });
        }
      });
    },


    //获取发布信息
    getTimeLineVoList(){
      let that = this;
      util.request(api.TimelineList, {
        page: that.data.timelineVoPage,
        limit: that.data.limit
      }).then(function(res) {
        if (res.errno === 0) {
          that.setData({
            timelineVoList: that.data.timelineVoList.concat(res.data.list),
            timelineVoTotalPages: res.data.pages
          });
        }
      });
    },

    //管理员发货
    wxShip:function(options){
      let that = this;
      let orderVo = options.target.dataset.ordervo;
      util.adminRequest(api.OrderAdminShip, {
        orderId: orderVo.id,
      },'POST').then(function(res) {
        console.log(res)
        if (res.errno === 0) {
          wx.showToast({
            title: '发货成功',
            icon: 'success',
            duration: 2000//持续的时间
          })
          that.getTimeLineList();
        }else{
          
          wx.showModal({
            title: '错误',
            content: res.errmsg,
            showCancel: false
          })
        }
      });

    },

    //管理员退款
    wxRefund:function(options){
      let orderVo = options.target.dataset.ordervo;
      util.adminRequest(api.OrderAdminRefund, {
        orderId: orderVo.id,
        refundMoney: orderVo.actualPrice,
      },'POST').then(function(res) {
        console.log(res)
        if (res.errno === 0) {
          wx.showToast({
            title: '退款成功',
            icon: 'success',
            duration: 2000//持续的时间
          })
        }else{
          util.showErrorToast(res.errmsg);
        }
      });
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      let that = this;
      that.getTimeLineVoList();
      that.getOrderVoList();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
      //获取用户的登录信息
      if (app.globalData.hasLogin) {
        let userInfo = wx.getStorageSync('userInfo');
        this.setData({
          userInfo: userInfo,
          hasLogin: true
        });
        //管理员登录
        if (app.globalData.Administrator) {
          this.setData({
            Administrator: true
          });
        }
      }
      
      this.getIndexData();
      //自定义底部导航栏高亮显示不加会导致高亮随机跳
      if (typeof this.getTabBar === 'function' && this.getTabBar()) {
        this.getTabBar().setData({
          selected: 1
        })
      }
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
      wx.showNavigationBarLoading() //在标题栏中显示加载
      this.getIndexData();
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    },

    getIndexData: function() {
      wx.showLoading({
        title: '加载中',
      });
  
      setTimeout(function() {
        wx.hideLoading()
      }, 2000);
  
      let that = this;
      if(that.data.TabCur == 0){
        //获取发布信息
        util.request(api.TimelineList).then(function(res) {
          if (res.errno === 0) {
            that.setData({
              timelineVoPage: 1,
              timelineVoList: res.data.list,
              timelineVoTotalPages: res.data.pages
            });
            wx.hideLoading();
          }
        });
      }else if(that.data.TabCur == 1){
        //获取订单信息
        util.request(api.TimelinOrderList, {
          isAdmin:that.data.Administrator,
        }).then(function(res) {
          if (res.errno === 0) {
            that.setData({
              orderVoPage: 1,
              orderVoList: res.data.list,
              orderVoTotalPages: res.data.pages
            });
            wx.hideLoading();
          }
        });
      }
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
      let that = this;
      if(that.data.TabCur == 0){
        //获取发布信息
        if (that.data.timelineVoTotalPages > that.data.timelineVoPage) {
          that.setData({
            timelineVoPage: that.data.timelineVoPage + 1
          });
          that.getTimeLineVoList();
        } else {
          wx.showToast({
            title: '没有更多信息了',
            icon: 'none',
            duration: 2000
          });
          return false;
        }
      }else if(that.data.TabCur == 1){
        //获取订单信息
        if (that.data.orderVoTotalPages > that.data.orderVoPage) {
          that.setData({
            orderVoPage: that.data.orderVoPage + 1
          });
          that.getOrderVoList();
        } else {
          wx.showToast({
            title: '没有更多信息了',
            icon: 'none',
            duration: 2000
          });
          return false;
        }
      }

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },

    goLogin() {
      if (!this.data.hasLogin) {
      wx.navigateTo({
          url: "/pages/auth/login/login"
      });
      }
    },
})