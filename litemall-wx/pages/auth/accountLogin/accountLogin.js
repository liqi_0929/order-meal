var api = require('../../../config/api.js');
var util = require('../../../utils/util.js');
var user = require('../../../utils/user.js');

var app = getApp();
Page({
  data: {
    username: '',
    password: '',
    code: '',
    loginErrorCount: 0,
    canIUseGetUserProfile: false,
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }

  },
  onReady: function() {

  },
  onShow: function() {
    // 页面显示
  },
  onHide: function() {
    // 页面隐藏
  },
  onUnload: function() {
    // 页面关闭
  },

  //获取授权
  wxLogin: function(e) {
    if(app.globalData.hasLogin){
      this.accountLogin();
      return;
    }
    if (this.data.canIUseGetUserProfile) {
      wx.getUserProfile({
        desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (res) => {
          this.doLogin(res.userInfo)
        },
        fail: () => {
          util.showErrorToast('管理员登录失败');
        }
      })
    }else {
      if (e.detail.userInfo == undefined) {
        app.globalData.hasLogin = false;
        app.globalData.Administrator = false;
        util.showErrorToast('管理员登录失败');
        return;
      }
      this.doLogin(e.detail.userInfo)
    }
  },

  //微信登陆
  doLogin: function(userInfo) {
    let that = this;
    user.checkLogin().catch(() => {
      user.loginByWeixin(userInfo).then(res => {
        app.globalData.hasLogin = true;
        that.accountLogin();
        return;
      }).catch((err) => {
        app.globalData.hasLogin = false;
        util.showErrorToast('微信授权失败');
      });

    });
  },

  //管理员登陆
  accountLogin: function() {
    var that = this;
    let username = that.data.username;
    let password = that.data.password;
    let code = that.data.code;

    if (password.length < 1 || username.length < 1) {
      wx.showModal({
        title: '错误信息',
        content: '请输入用户名和密码',
        showCancel: false
      });
      return false;
    }
    if (code.length < 1) {
      wx.showModal({
        title: '错误信息',
        content: '请输入验证码',
        showCancel: false
      });
      return false;
    }

    user.loginByAccount(username,password,code).then(res => {
      app.globalData.Administrator = true;
      wx.switchTab({
        url: '/pages/index/index',
      })
      return;
    }).catch((err) => {
      app.globalData.Administrator = false;
      wx.showModal({
        title: '错误信息',
        content: err.errmsg,
        showCancel: false
      });
    });
  },

  bindUsernameInput: function(e) {
    this.setData({
      username: e.detail.value
    });
  },

  bindPasswordInput: function(e) {
    this.setData({
      password: e.detail.value
    });
  },

  bindCodeInput: function(e) {
    this.setData({
      code: e.detail.value
    });
  },

  sendCode: function() {
    let that = this;
    if (this.data.password.length < 1 || this.data.username.length < 1) {
      wx.showModal({
        title: '错误信息',
        content: '请输入用户名和密码',
        showCancel: false
      });
      return false;
    }
    util.request(api.AuthMailCaptcha, {
      username: that.data.username
    },'POST').then(function(res) {
      console.log(res)
      if (res.errno === 0) {
        wx.showModal({
          title: '发送成功',
          content: res.data,
          showCancel: false
        });
      }else{
        wx.showModal({
          title: '错误信息',
          content: res.errmsg,
          showCancel: false
        });
      }
    });
  },

  clearInput: function(e) {
    switch (e.currentTarget.id) {
      case 'clear-username':
        this.setData({
          username: ''
        });
        break;
      case 'clear-password':
        this.setData({
          password: ''
        });
        break;
      case 'clear-code':
        this.setData({
          code: ''
        });
        break;
    }
  }
})