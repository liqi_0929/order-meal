const util = require('../../../utils/util.js');
const api = require('../../../config/api.js');
const DatePickerUtil = require('../../../utils/DatePicker.js');

const app = getApp();

Page({
  data: {
    orderType: '',
    time:'选择预约时间',
    multiArray:[],//piker的item项
    multiIndex:[],//当前选择列的下标
    year:'',//选择的年
    month:'',//选择的月
    day:'',//选择的日
    hour:'',//选择的时
    minute:'',//选择的分
    checkedGoodsList: [],
    checkedAddress: {},
    checkedCoupon: [],
    goodsTotalPrice: 0.00, //商品总价
    availableCouponLength: 0, // 可用的优惠券数量
    freightPrice: 0.00, //快递费
    couponPrice: 0.00, //优惠券的价格
    grouponPrice: 0.00, //团购优惠价格
    orderTotalPrice: 0.00, //订单总价
    actualPrice: 0.00, //实际需要支付的总价
    cartId: 0,
    addressId: 0,
    couponId: 0,
    userCouponId: 0,
    message: '',
    grouponLinkId: 0, //参与的团购，如果是发起则为0
    grouponRulesId: 0 //团购规则ID
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    //获取 DatePickerUtil 工具下的方法
    var  loadPickerData=DatePickerUtil.loadPickerData()
    var  getCurrentDate=DatePickerUtil.getCurrentDate()
    var  GetMultiIndex=DatePickerUtil.GetMultiIndex() 
    
    //这里写的是为了记录当前时间
    let year=parseInt(getCurrentDate.substring(0,4)); 
    let month=parseInt(getCurrentDate.substring(5,7)); 
    let day=parseInt(getCurrentDate.substring(8,10)); 
    let hour=parseInt(getCurrentDate.substring(11,13));   
    let minute=parseInt(getCurrentDate.substring(14,16)); 
    
    this.setData({  
      multiArray:loadPickerData,//picker数组赋值，格式 [years, months, days, hours, minutes]
      multiIndex:GetMultiIndex,//设置pickerIndex，[0,0,0,0,0]
      // time:getCurrentDate, //设置当前时间 ，currentYears+'-'+mm+'-'+dd+' '+hh+':'+min
      year:year,//记录选择的年
      month:month,//记录选择的月
      day:day,//记录选择的日
      hour:hour,//记录选择的时
      minute:minute,//记录选择的分 
      orderType : app.globalData.orderType, //订单类型
    });  
  },
  

  //获取checkou信息
  getCheckoutInfo: function() {
    let that = this;
    util.request(api.CartCheckout, {
      cartId: that.data.cartId,
      addressId: that.data.addressId,
      userCouponId: that.data.userCouponId,
      couponId: that.data.couponId,
      grouponRulesId: that.data.grouponRulesId
    }).then(function(res) {
      if (res.errno === 0) {
        that.setData({
          checkedGoodsList: res.data.checkedGoodsList,
          checkedAddress: res.data.checkedAddress,
          actualPrice: res.data.actualPrice,
          checkedCoupon: res.data.checkedCoupon,
          availableCouponLength: res.data.availableCouponLength,
          couponPrice: res.data.couponPrice,
          grouponPrice: res.data.grouponPrice,
          freightPrice: res.data.freightPrice,
          goodsTotalPrice: res.data.goodsTotalPrice,
          orderTotalPrice: res.data.orderTotalPrice,
          addressId: res.data.addressId,
          couponId: res.data.couponId,
          userCouponId: res.data.userCouponId,
          grouponRulesId: res.data.grouponRulesId,
        });
      }
      wx.hideLoading();
    });
  },
  selectAddress() {
    wx.navigateTo({
      url: '/pages/ucenter/address/address',
    })
  },
  selectCoupon() {
    wx.navigateTo({
      url: '/pages/ucenter/couponSelect/couponSelect',
    })
  },
  bindMessageInput: function(e) {
    this.setData({
      message: e.detail.value
    });
  },

  bindDateChange: function(e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      date: e.detail.value
    })
  },
  bindTimeChange: function(e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      time: e.detail.value
    })
  },
  onReady: function() {
    // 页面渲染完成

  },
  onShow: function() {
    // 页面显示
    wx.showLoading({
      title: '加载中...',
    });
    try {
      var cartId = wx.getStorageSync('cartId');
      if (cartId === "") {
        cartId = 0;
      }
      var addressId = wx.getStorageSync('addressId');
      if (addressId === "") {
        addressId = 0;
      }
      var couponId = wx.getStorageSync('couponId');
      if (couponId === "") {
        couponId = 0;
      }
      var grouponRulesId = wx.getStorageSync('grouponRulesId');
      if (grouponRulesId === "") {
        grouponRulesId = 0;
      }
      var userCouponId = wx.getStorageSync('userCouponId');
      if (userCouponId === "") {
        userCouponId = 0;
      }
      var grouponLinkId = wx.getStorageSync('grouponLinkId');
      if (grouponLinkId === "") {
        grouponLinkId = 0;
      }
      this.setData({
        cartId: cartId,
        addressId: addressId,
        couponId: couponId,
        userCouponId: userCouponId,
        grouponRulesId: grouponRulesId,
        grouponLinkId: grouponLinkId
      });
    } catch (e) {
      // Do something when catch error
      console.log(e);
    }
    this.getCheckoutInfo();
  },
  onHide: function() {
    // 页面隐藏

  },
  onUnload: function() {
    // 页面关闭

  },
  submitOrder: function() {
    var that = this , orderTypeDate;
    var orderType = app.globalData.orderType;
    switch (orderType) {
      case "预约模式":
        if(that.data.time=="选择预约时间"){
          util.showErrorToast('请选择预约时间');
          return false;
        }
        orderTypeDate = that.data.time;
        orderType = "到店日期：";
        break;
      case "到店点餐":
        orderTypeDate = orderType;
        orderType = "订单模式：";
        break;
      default:
        orderTypeDate = orderType;
        orderType = "座位号："
        break;
    }

    if(that.data.addressId<=0){
      that.setData({
        addressId : -1
      })
    }
    
    util.request(api.OrderSubmit, {
      cartId: that.data.cartId,
      orderType: orderType,
      addressId: that.data.addressId,
      couponId: that.data.couponId,
      userCouponId: that.data.userCouponId,
      orderTypeDate: orderTypeDate,
      message: that.data.message,
      grouponRulesId: that.data.grouponRulesId,
      grouponLinkId: that.data.grouponLinkId
    }, 'POST').then(res => {
      if (res.errno === 0) {
        // 下单成功，重置couponId
        try {
          wx.setStorageSync('couponId', 0);
        } catch (error) {
          console.log(error)
        }
        const orderId = res.data.orderId;
        const grouponLinkId = res.data.grouponLinkId;
        util.request(api.OrderPrepay, {
          orderId: orderId
        }, 'POST').then(function(res) {
          if (res.errno === 0) {
            const payParam = res.data;
            console.log("支付过程开始");
            wx.requestPayment({
              'timeStamp': payParam.timeStamp,
              'nonceStr': payParam.nonceStr,
              'package': payParam.packageValue,
              'signType': payParam.signType,
              'paySign': payParam.paySign,
              'success': function(res) {
                console.log("支付过程成功");
                if (grouponLinkId) {
                  setTimeout(() => {
                    wx.redirectTo({
                      url: '/pages/groupon/grouponDetail/grouponDetail?id=' + grouponLinkId
                    })
                  }, 1000);
                } else {
                  wx.redirectTo({
                    url: '/pages/payResult/payResult?status=1&orderId=' + orderId
                  });
                }
              },
              'fail': function(res) {
                console.log("支付过程失败");
                wx.redirectTo({
                  url: '/pages/payResult/payResult?status=0&orderId=' + orderId
                });
              },
              'complete': function(res) {
                console.log("支付过程结束")
              }
            });
          } else {
            wx.redirectTo({
              url: '/pages/payResult/payResult?status=0&orderId=' + orderId
            });
          }
        });

      } else {
        util.showErrorToast(res.errmsg);
      }
    });
  },

  bindMultiPickerChange: function(e) { //时间日期picker选择改变后，点击确定 
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      multiIndex: e.detail.value
    })
    const index = this.data.multiIndex; // 当前选择列的下标
    const year = this.data.multiArray[0][index[0]];
    const month = this.data.multiArray[1][index[1]];
    const day = this.data.multiArray[2][index[2]];
    const hour = this.data.multiArray[3][index[3]];
    const minute = this.data.multiArray[4][index[4]];
    // console.log(`${year}-${month}-${day} ${hour}:${minute}`); 
    if(!hour){
      util.showErrorToast('当前时间不可预约');
      return false;
    }
    this.setData({
      time: year + month + day + hour + minute,
      year:year, //记录选择的年
      month:month, //记录选择的月
      day:day, //记录选择的日
      hour:hour, //记录选择的时
      minute:minute, //记录选择的分 
    })
    // console.log(this.data.time); 
  }, 

  bindMultiPickerColumnChange: function(e) { //监听picker的滚动事件
    // console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    let getCurrentDate = DatePickerUtil.getCurrentDate();//获取现在时间  
    let currentYear = parseInt(getCurrentDate.substring(0,4)); 
    let currentMonth = parseInt(getCurrentDate.substring(5,7)); 
    let currentDay = parseInt(getCurrentDate.substring(8,10)); 
    let currentHour = parseInt(getCurrentDate.substring(11,13));  
    let currentMinute = parseInt(getCurrentDate.substring(14,16)); 
    
    if (e.detail.column == 0) {//修改年份列   
      let yearSelected = parseInt(this.data.multiArray[e.detail.column][e.detail.value]);//当前选择的年份
      this.setData({ 
        multiIndex:[0,0,0,0,0] ,//设置pickerIndex
        year:yearSelected //当前选择的年份
      });
      
      if(yearSelected == currentYear){//当前选择的年份==当前年份  
        var loadPickerData=DatePickerUtil.loadPickerData();
        this.setData({
          multiArray:loadPickerData,//picker数组赋值
          multiIndex:[0,0,0,0,0] //设置pickerIndex
        });
        
      }else{  // 选择的年份！=当前年份 
        // 处理月份
        let monthArr = DatePickerUtil.loadMonths(1,12)
        // 处理日期
        let dayArr = DatePickerUtil.loadDays(currentYear,currentMonth,1) 
        // 处理hour
        let  hourArr = DatePickerUtil.loadHours(10,20); 
        // 处理minute
        let  minuteArr = DatePickerUtil.loadMinutes(0,60)
        // 给每列赋值回去
        this.setData({
          ['multiArray[1]']: monthArr,
          ['multiArray[2]']: dayArr,
          ['multiArray[3]']: hourArr,
          ['multiArray[4]']: minuteArr
        });
      }
    }
    if (e.detail.column == 1) {//修改月份列
      let mon = parseInt(this.data.multiArray[e.detail.column][e.detail.value]); //当前选择的月份
      this.setData({
        month:mon  // 记录当前列
      })
      if(mon==currentMonth){//选择的月份==当前月份 
        if(this.data.year==currentYear){  
          // 处理日期
          let dayArr=DatePickerUtil.loadDays(currentYear,mon,currentDay) 
          // // 处理hour
          // let  hourArr=DatePickerUtil.loadHours(currentHour,20); 
          // 处理hour
          let  hourArr=DatePickerUtil.loadHours(10,20); 
          // 处理minute
          let  minuteArr=DatePickerUtil.loadMinutes(currentMinute,60)

          this.setData({ 
            ['multiArray[2]']:dayArr,
            ['multiArray[3]']: hourArr,
            ['multiArray[4]']: minuteArr
          })
        }else{ 
          // 处理日期
          let dayArr=DatePickerUtil.loadDays(currentYear,mon,1) 
          // 处理hour
          let  hourArr=DatePickerUtil.loadHours(10,20); 
          // 处理minute
          let  minuteArr=DatePickerUtil.loadMinutes(0,60)
          
          this.setData({
            ['multiArray[2]']:dayArr,
            ['multiArray[3]']: hourArr,
            ['multiArray[4]']: minuteArr
          });
        } 
      }else{  // 选择的月份！=当前月份 
         // 处理日期
         let dayArr = DatePickerUtil.loadDays(currentYear,mon,1) // 传入当前年份，当前选择的月份去计算日
         // 处理hour
         let  hourArr = DatePickerUtil.loadHours(10,20); 
         // 处理minute
         let  minuteArr = DatePickerUtil.loadMinutes(0,60)
         
       	 this.setData({
           ['multiArray[2]']:dayArr,
           ['multiArray[3]']: hourArr,
           ['multiArray[4]']: minuteArr
         });
      } 
    } 
    if(e.detail.column == 2) {//修改日
      let dd = parseInt(this.data.multiArray[e.detail.column][e.detail.value]);//当前选择的日
      this.setData({
        day:dd
      })
      if(dd==currentDay){//选择的日==当前日
        if(this.data.year==currentYear&&this.data.month==currentMonth){//选择的是今天 
        
         // 处理hour
         let  hourArr=DatePickerUtil.loadHours(currentHour,20); 
         // 处理minute
         let  minuteArr=DatePickerUtil.loadMinutes(currentMinute,60)
         
         this.setData({
            ['multiArray[3]']: hourArr,
            ['multiArray[4]']: minuteArr
         });
         
        }else{ //选择的不是今天 
          // 处理hour
          let  hourArr=DatePickerUtil.loadHours(10,20); 
          // 处理minute
          let  minuteArr=DatePickerUtil.loadMinutes(0,60)
          
          this.setData({
             ['multiArray[3]']: hourArr,
             ['multiArray[4]']: minuteArr
          });
        }
      }else{  // 选择的日！=当前日 
       // 处理hour
       let  hourArr = DatePickerUtil.loadHours(10,20); 
       // 处理minute
       let  minuteArr=DatePickerUtil.loadMinutes(0,60)
       
       this.setData({
         ['multiArray[3]']: hourArr,
         ['multiArray[4]']: minuteArr
       });
      }
    } 
    if(e.detail.column == 3) {//修改时
      let hh = parseInt(this.data.multiArray[e.detail.column][e.detail.value]); //当前选择的时
      this.setData({
        hour:hh
      })
      if(hh==currentHour){//选择的时==当前时 
        if(this.data.year==currentYear&&this.data.month==currentMonth&&this.data.month==currentMonth){   // 选择的是今天
          
          // 处理minute
            let  minuteArr=DatePickerUtil.loadMinutes(currentMinute,60)
            this.setData({ 
              ['multiArray[4]']: minuteArr
            });
        }else{ // 选择的不是今天
        
          // 处理minute
          let  minuteArr=DatePickerUtil.loadMinutes(0,60)
          this.setData({ 
            ['multiArray[4]']: minuteArr
          });
        } 
      }else{//选择的时！=当前时 
        // 处理minute
        let  minuteArr=DatePickerUtil.loadMinutes(0,60)
        this.setData({ 
          ['multiArray[4]']: minuteArr
        });
      }
    }
    var data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex
    };

    // console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    data.multiIndex[e.detail.column] = e.detail.value; //将值赋回去
    
    this.setData(data);  //将值赋回去
  }
});